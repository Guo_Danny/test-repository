﻿
namespace MQTest2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtQueueManagerName = new System.Windows.Forms.TextBox();
            this.txtChannelInfo = new System.Windows.Forms.TextBox();
            this.txtPUTQueueName = new System.Windows.Forms.TextBox();
            this.txtPUT = new System.Windows.Forms.TextBox();
            this.txtGETQueueName = new System.Windows.Forms.TextBox();
            this.txtGET = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnWriteMsg = new System.Windows.Forms.Button();
            this.btnReadMsg = new System.Windows.Forms.Button();
            this.lblConnect = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "QueueMangerName";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "ChannelInfo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(155, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "QueueToSendName";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 305);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "QueueToReceiveName";
            // 
            // txtQueueManagerName
            // 
            this.txtQueueManagerName.Location = new System.Drawing.Point(240, 37);
            this.txtQueueManagerName.Name = "txtQueueManagerName";
            this.txtQueueManagerName.Size = new System.Drawing.Size(211, 26);
            this.txtQueueManagerName.TabIndex = 4;
            // 
            // txtChannelInfo
            // 
            this.txtChannelInfo.Location = new System.Drawing.Point(240, 98);
            this.txtChannelInfo.Name = "txtChannelInfo";
            this.txtChannelInfo.Size = new System.Drawing.Size(211, 26);
            this.txtChannelInfo.TabIndex = 5;
            // 
            // txtPUTQueueName
            // 
            this.txtPUTQueueName.Location = new System.Drawing.Point(240, 203);
            this.txtPUTQueueName.Name = "txtPUTQueueName";
            this.txtPUTQueueName.Size = new System.Drawing.Size(211, 26);
            this.txtPUTQueueName.TabIndex = 6;
            // 
            // txtPUT
            // 
            this.txtPUT.Location = new System.Drawing.Point(57, 251);
            this.txtPUT.Name = "txtPUT";
            this.txtPUT.Size = new System.Drawing.Size(539, 26);
            this.txtPUT.TabIndex = 7;
            // 
            // txtGETQueueName
            // 
            this.txtGETQueueName.Location = new System.Drawing.Point(240, 302);
            this.txtGETQueueName.Name = "txtGETQueueName";
            this.txtGETQueueName.Size = new System.Drawing.Size(211, 26);
            this.txtGETQueueName.TabIndex = 8;
            // 
            // txtGET
            // 
            this.txtGET.Location = new System.Drawing.Point(57, 368);
            this.txtGET.Name = "txtGET";
            this.txtGET.Size = new System.Drawing.Size(539, 26);
            this.txtGET.TabIndex = 9;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(521, 60);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(151, 50);
            this.btnConnect.TabIndex = 10;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnWriteMsg
            // 
            this.btnWriteMsg.Location = new System.Drawing.Point(622, 227);
            this.btnWriteMsg.Name = "btnWriteMsg";
            this.btnWriteMsg.Size = new System.Drawing.Size(151, 50);
            this.btnWriteMsg.TabIndex = 11;
            this.btnWriteMsg.Text = "Write Messages";
            this.btnWriteMsg.UseVisualStyleBackColor = true;
            this.btnWriteMsg.Click += new System.EventHandler(this.btnWriteMsg_Click);
            // 
            // btnReadMsg
            // 
            this.btnReadMsg.Location = new System.Drawing.Point(622, 344);
            this.btnReadMsg.Name = "btnReadMsg";
            this.btnReadMsg.Size = new System.Drawing.Size(151, 50);
            this.btnReadMsg.TabIndex = 12;
            this.btnReadMsg.Text = "Read Messages";
            this.btnReadMsg.UseVisualStyleBackColor = true;
            this.btnReadMsg.Click += new System.EventHandler(this.btnReadMsg_Click);
            // 
            // lblConnect
            // 
            this.lblConnect.AutoSize = true;
            this.lblConnect.Location = new System.Drawing.Point(53, 156);
            this.lblConnect.Name = "lblConnect";
            this.lblConnect.Size = new System.Drawing.Size(51, 20);
            this.lblConnect.TabIndex = 13;
            this.lblConnect.Text = "label5";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblConnect);
            this.Controls.Add(this.btnReadMsg);
            this.Controls.Add(this.btnWriteMsg);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.txtGET);
            this.Controls.Add(this.txtGETQueueName);
            this.Controls.Add(this.txtPUT);
            this.Controls.Add(this.txtPUTQueueName);
            this.Controls.Add(this.txtChannelInfo);
            this.Controls.Add(this.txtQueueManagerName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtQueueManagerName;
        private System.Windows.Forms.TextBox txtChannelInfo;
        private System.Windows.Forms.TextBox txtPUTQueueName;
        private System.Windows.Forms.TextBox txtPUT;
        private System.Windows.Forms.TextBox txtGETQueueName;
        private System.Windows.Forms.TextBox txtGET;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnWriteMsg;
        private System.Windows.Forms.Button btnReadMsg;
        private System.Windows.Forms.Label lblConnect;
    }
}

