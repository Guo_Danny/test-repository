﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace MatchGame2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Image[] imgs;
        Random random = new Random();
        List<string> icons = new List<string>()
        {
            "\\1.png","\\2.png","\\3.png","\\4.png",
            "\\5.png","\\6.png","\\7.png","\\8.png",
            "\\9.png","\\10.png","\\11.png","\\12.png",
            "\\13.png","\\14.png","\\15.png","\\16.png"
        };
    private void Form1_Load(object sender, EventArgs e)
        {
            imgs = new Image[16];

            string p = Directory.GetCurrentDirectory();

            foreach (Control control in tableLayoutPanel1.Controls)
            {
                PictureBox iconpic = control as PictureBox;
                if (iconpic != null)
                {
                    iconpic.Image = Image.FromFile(p + "\\pic\\0.png");
                }
            }
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            PictureBox clickedpic = sender as PictureBox;

            if (clickedpic != null)
            {
                string p = Directory.GetCurrentDirectory();
                // If the clicked label is black, the player clicked
                // an icon that's already been revealed --
                // ignore the click
                int randomNumber = random.Next(icons.Count);
                clickedpic.Image = Image.FromFile(p + "\\pic" + icons[randomNumber]);
                icons.RemoveAt(randomNumber);
            }
        }

        private void tableLayoutPanel1_Click(object sender, EventArgs e)
        {

        }
    }
}
