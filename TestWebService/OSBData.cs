﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Data;
namespace TestWebService
{
    public class OSBData
    {
        Database database;

        public DataSet QueryStatus(string input_TRANSACTIONID)
        {
            try
            {
                DataSet ds = new DataSet();

                database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                string SQL = @"
                select * from (
                select a.SeqNumb, a.Ref,case when isnull(a.ConfirmState,0) = 2 and a.app = 0 then 'R'
                when isnull(a.ConfirmState,0) = 2 and a.app = 1 then 'A' 
                when isnull(a.ConfirmState,0) = 1 and a.app = 0 then 'R'
                when isnull(a.ConfirmState,0) = 1 and a.app = 1 then 'B'
                when isnull(a.ConfirmState,0) = 0 then 'U'
                else 'N'
                end scanresult 
                from ofac..FilterTranTable(nolock) a
                union
                select b.SeqNumb, b.Ref,case when isnull(b.ConfirmState,0) = 2 and b.app = 0 then 'R'
                when isnull(b.ConfirmState,0) = 2 and b.app = 1 then 'A' 
                when isnull(b.ConfirmState,0) = 1 and b.app = 0 then 'R'
                when isnull(b.ConfirmState,0) = 1 and b.app = 1 then 'B'
                when isnull(b.ConfirmState,0) = 0 then 'U'
                else 'N'
                end scanresult 
                from ofac..FilterTranhistTable(nolock) b
                ) T 
                where CHARINDEX(@ref, T.Ref) > 0 order by SeqNumb desc
                ";

                using (DbCommand dBCommand = database.GetSqlStringCommand(SQL))
                {
                    database.AddInParameter(dBCommand, "@ref", DbType.String, input_TRANSACTIONID.Trim());

                    try
                    {
                        //May. 09, 2022 add exception catch
                        ds = database.ExecuteDataSet(dBCommand);

                        if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                        {
                            return ds;
                        }
                    }

                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        throw e;
                    }
                    finally
                    {
                        dBCommand.Dispose();
                    }
                }

                return ds;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool CheckUser(string input_USERID)
        {
            try
            {
                DataSet ds = new DataSet();
                database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
                bool bol_return = false;

                string SQL = @"
                select * from psec..Operator 
                where [Enable] = 1 and DeleteF = 0 and Approved = 1 
                and UserName = @Userid;
                ";

                using (DbCommand dBCommand = database.GetSqlStringCommand(SQL))
                {
                    database.AddInParameter(dBCommand, "@Userid", DbType.String, input_USERID.Trim());

                    try
                    {
                        //May. 09, 2022 add exception catch
                        ds = database.ExecuteDataSet(dBCommand);

                        if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0 && ds.Tables[0].Rows[0]["Code"].ToString() != "")
                        {
                            bol_return = true;
                        }
                    }

                    catch (Exception e)
                    {
                        throw e;
                    }
                    finally
                    {
                        dBCommand.Dispose();
                    }
                }

                return bol_return;
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public bool CheckBranch(string input_BRANCH)
        {
            try
            {
                DataSet ds = new DataSet();
                database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
                bool bol_return = false;

                string SQL = @"
                    select * from Psec..Branch
                    where Code = @Code;
                ";

                using (DbCommand dBCommand = database.GetSqlStringCommand(SQL))
                {
                    database.AddInParameter(dBCommand, "@Code", DbType.String, input_BRANCH.Trim());

                    try
                    {
                        //May. 09, 2022 add exception catch
                        ds = database.ExecuteDataSet(dBCommand);

                        if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0 && ds.Tables[0].Rows[0]["Code"].ToString() != "")
                        {
                            bol_return = true;
                        }
                    }

                    catch (Exception e)
                    {
                        throw e;
                    }
                    finally
                    {
                        dBCommand.Dispose();
                    }
                }

                return bol_return;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool CheckOrg(string input_ORGANIZATION)
        {
            bool bol_return = false;
            try
            {
                if (input_ORGANIZATION.Trim().ToUpper() == "BOTOSB")
                {
                    bol_return = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return bol_return;
        }

        public string Check()
        {
            string str_return = "";



            return str_return;
        }
    }
}