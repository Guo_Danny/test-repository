﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWebService
{
    public class OSB_QUERYRESULT
    {
        public string ORGANIZATION { get; set; }
        public string BRANCH { get; set; }
        public string USERID { get; set; }
        public string PASSWORD { get; set; }
        public string TRANSACTIONTM { get; set; }
        public string TRANSACTIONID { get; set; }
        public string RESULTCODE { get; set; }
        public string RESULTMESSAGE { get; set; }
    }
}