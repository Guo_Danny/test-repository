﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClientTest.aspx.cs" Inherits="TestWebService.ClientTest" validateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td><asp:Label ID="L_1" runat="server" Text="Transaction Id:"></asp:Label></td><td><asp:TextBox ID="TextBox1" runat="server" Width="375px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td><td><asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Query Status" /></td>
                </tr>
                <tr>
                    <td><asp:Label ID="L_query" Text="Query Result: " runat="server"></asp:Label></td><td><asp:Label ID="L_resultcode" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label1" runat="server" Text="Organization:"></asp:Label></td><td><asp:TextBox ID="TextBox2" runat="server" Width="375px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label2" runat="server" Text="Branch:"></asp:Label></td><td><asp:TextBox ID="TextBox3" runat="server" Width="375px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label3" runat="server" Text="UserId:"></asp:Label></td><td><asp:TextBox ID="TextBox4" runat="server" Width="375px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label4" runat="server" Text="Password:"></asp:Label></td><td><asp:TextBox ID="TextBox5" runat="server" TextMode="Password" Width="375px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label5" runat="server" Text="Transaction time:"></asp:Label></td><td><asp:TextBox ID="TextBox6" runat="server" Width="375px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label6" runat="server" Text="Transaction id:"></asp:Label></td><td><asp:TextBox ID="TextBox7" runat="server" width="375px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label7" runat="server" Text="FormatType:"></asp:Label></td><td><asp:TextBox ID="TextBox8" runat="server" width="375px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label8" runat="server" Text="TransactionText:"></asp:Label></td><td><asp:TextBox ID="TextBox9" runat="server" TextMode="MultiLine" Width="375px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label9" runat="server" Text="Scan Name1:"></asp:Label></td><td><asp:TextBox ID="txt_name1" runat="server" Width="375px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label10" runat="server" Text="Scan Name2:"></asp:Label></td><td><asp:TextBox ID="txt_name2" runat="server" Width="375px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td><td><asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Send Scan" /></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label11" runat="server" Text="Return Code:"></asp:Label></td><td><asp:Label ID="L_ReturnCode" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label12" runat="server" Text="Return Message:"></asp:Label></td><td><asp:Label ID="L_ReturnMessage" runat="server"></asp:Label></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
