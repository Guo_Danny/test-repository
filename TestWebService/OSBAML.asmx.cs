﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.IO;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Data;
namespace TestWebService
{
    /// <summary>
    /// Summary description for OSBAML
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class OSBAML : System.Web.Services.WebService
    {
        public OSB_QUERYRESULT R_StatusReturn = new OSB_QUERYRESULT();
        public OSB_SCANRESULT R_ScanReturn = new OSB_SCANRESULT();

        [WebMethod]
        public OSB_QUERYRESULT OSB_Query(string input_ORGANIZATION, string input_BRANCH, string input_USERID, string input_PASSWORD, string input_TRANSACTIONTM, string input_TRANSACTIONID)
        {
            try
            {
                OSBData OSBdb = new OSBData();

                R_StatusReturn.ORGANIZATION = input_ORGANIZATION;
                R_StatusReturn.BRANCH = input_BRANCH;
                R_StatusReturn.USERID = input_USERID;
                R_StatusReturn.PASSWORD = input_PASSWORD;
                R_StatusReturn.TRANSACTIONTM = input_TRANSACTIONTM;
                R_StatusReturn.TRANSACTIONID = input_TRANSACTIONID;
                R_StatusReturn.RESULTMESSAGE = "";
                R_StatusReturn.RESULTCODE = "S";

                string strRESULTMESSAGE = "S";

                //check input data
                if (!checkinput(input_USERID, input_BRANCH, input_ORGANIZATION, ref strRESULTMESSAGE))
                {
                    R_StatusReturn.RESULTMESSAGE = strRESULTMESSAGE;
                    return R_StatusReturn;
                }

                //query case status
                DataSet ds = OSBdb.QueryStatus(input_TRANSACTIONID);

                if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                {
                    R_StatusReturn.RESULTCODE = ds.Tables[0].Rows[0]["scanresult"].ToString();
                }

            }
            catch (Exception e)
            {
                R_StatusReturn.RESULTMESSAGE = e.ToString();
            }
            return R_StatusReturn;
        }

        [WebMethod]
        public OSB_SCANRESULT OSB_Scan(string input_ORGANIZATION, string input_BRANCH, string input_USERID, string input_PASSWORD, string input_TRANSACTIONTM, string input_TRANSACTIONID, string input_FORMATTYPE, string input_TRANSACTIONTEXT)
        {
            try
            {
                OSBData OSBdb = new OSBData();
                string str_inputpath = ConfigurationManager.AppSettings["FileInPath"];
                string str_delayseconds = ConfigurationManager.AppSettings["WaitForScan"];
                //str_inputpath = str_inputpath + "\\" + input_BRANCH + "SR" + input_TRANSACTIONTM.Substring(0, 8) + "103" + " 000001 " + DateTime.Now.Hour.ToString().PadRight(6, '0');
                str_inputpath = str_inputpath + "\\" + input_TRANSACTIONID;

                R_ScanReturn.ORGANIZATION = input_ORGANIZATION;
                R_ScanReturn.BRANCH = input_BRANCH;
                R_ScanReturn.USERID = input_USERID;
                R_ScanReturn.PASSWORD = input_PASSWORD;
                R_ScanReturn.TRANSACTIONTM = input_TRANSACTIONTM;
                R_ScanReturn.TRANSACTIONID = input_TRANSACTIONID;
                R_ScanReturn.FORMATTYPE = input_FORMATTYPE;
                R_ScanReturn.RESULTCODE = "Y";
                R_ScanReturn.RESULTMESSAGE = "";

                string strRESULTMESSAGE = "";

                //check input data
                if (!checkinput(input_USERID, input_BRANCH, input_ORGANIZATION, ref strRESULTMESSAGE))
                {
                    R_ScanReturn.RESULTMESSAGE = strRESULTMESSAGE;
                    return R_ScanReturn;
                }
                else
                {
                    R_ScanReturn.RESULTMESSAGE = "";
                }

                StreamWriter sw = new StreamWriter(str_inputpath);

                sw.Write("}}{4:" + input_TRANSACTIONTEXT + "-}");

                sw.Close();


                //query case status
                //delay and wait for screen process.
                int idelyseconds = 10;
                if (string.IsNullOrEmpty(str_delayseconds) || !int.TryParse(str_delayseconds,out idelyseconds))
                {
                    idelyseconds = 10;
                }
                System.Threading.Thread.Sleep(idelyseconds * 1000);

                DataSet ds = OSBdb.QueryStatus(input_TRANSACTIONID);

                if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                {
                    R_ScanReturn.RESULTCODE = "Y";
                }
                else
                {
                    R_ScanReturn.RESULTCODE = "N";
                }

                return R_ScanReturn;
            }
            catch (Exception e)
            {
                R_ScanReturn.RESULTMESSAGE = e.Message.ToString();
                return R_ScanReturn;
            }
        }

        public bool checkinput(string input_USERID, string input_BRANCH, string input_ORGANIZATION, ref string RESULTMESSAGE)
        {
            bool bol_r = false;
            OSBData OSBdb = new OSBData();

            //check user id
            if (!OSBdb.CheckUser(input_USERID))
            {
                RESULTMESSAGE = "Invalid User ID";
                bol_r = false;
                return bol_r;
            }
            else
            {
                bol_r = true;
            }

            //check Branch
            if (!OSBdb.CheckBranch(input_BRANCH))
            {
                RESULTMESSAGE = "Invalid Branch";
                bol_r = false;
                return bol_r;
            }
            else
            {
                bol_r = true;
            }

            //check orgnization
            if (!OSBdb.CheckOrg(input_ORGANIZATION))
            {
                RESULTMESSAGE = "Invalid Orgnization";
                bol_r = false;
            }
            else
            {
                bol_r = true;
            }

            return bol_r;
        }
    }
}
