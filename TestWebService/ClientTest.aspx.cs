﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
namespace TestWebService
{
    public partial class ClientTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TextBox2.Text = "botosb";
                //TextBox3.Text = "0083";
                TextBox4.Text = "primeadmin";
                TextBox5.Text = "Passw0rd";
                TextBox6.Text = DateTime.Now.ToString();

                TextBox7.Text = "SR" + DateTime.Now.ToString("yyyyMMdd") + "103 000107 000024";

                TextBox8.Text = "MX";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            OSBAML obj = new OSBAML();
            //OSBAML.OSB_QUERYRESULT objRes = new OSBAML.OSB_QUERYRESULT();
            OSB_QUERYRESULT objRes = new OSB_QUERYRESULT();

            objRes = obj.OSB_Query(TextBox2.Text, TextBox3.Text, TextBox4.Text, TextBox5.Text, TextBox6.Text, TextBox1.Text);

            L_resultcode.Text = objRes.RESULTCODE;
            L_ReturnMessage.Text = objRes.RESULTMESSAGE;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            OSBAML obj = new OSBAML();
            //OSBAML.OSB_SCANRESULT objRes = new OSBAML.OSB_SCANRESULT();
            OSB_SCANRESULT objRes = new OSB_SCANRESULT();
            string str_input = @"
<AppHdr xmlns=""urn: iso: std: iso: 20022:tech: xsd: head.001.001.02"">
    <Fr>
        <FIId>
            <FinInstnId>
                <BICFI>AABBGB2X</BICFI>
            </FinInstnId>
        </FIId>
    </Fr>
    <To>
        <FIId>
            <FinInstnId>
                <BICFI>AABBIE2X</BICFI>
            </FinInstnId>
        </FIId>
    </To>
    <BizMsgIdr>Buzmsgidr/pacc4</BizMsgIdr>
    <MsgDefIdr>Msgdefidr/acs.004/001</MsgDefIdr>
    <BizSvc>swift.cbprplus.02</BizSvc>
    <CreDt>2022-03-10T09:24:41+01:00</CreDt>
</AppHdr>
<Document xmlns=""urn: iso: std: iso: 20022:tech: xsd: pacs.004.001.09"">
    <PmtRtr>
        <GrpHdr>
            <MsgId>Buzmsgidr/pacc4</MsgId>
            <CreDtTm>2022-03-15T10:26:17+01:00</CreDtTm>
            <NbOfTxs>1</NbOfTxs>
            <SttlmInf>
                <SttlmMtd>INDA</SttlmMtd>
            </SttlmInf>
        </GrpHdr>
        <TxInf>
            <OrgnlInstrId>pacs8gidr02</OrgnlInstrId>
            <OrgnlEndToEndId>Orgnle2eidpacs9</OrgnlEndToEndId>
            <OrgnlUETR>3f91d85a-48e0-4410-9b5c-8023c90eb587</OrgnlUETR>
            <OrgnlIntrBkSttlmAmt Ccy=""EUR"">150.</OrgnlIntrBkSttlmAmt>
            <RtrdIntrBkSttlmAmt Ccy=""EUR"">100.</RtrdIntrBkSttlmAmt>
            <IntrBkSttlmDt>2022-03-09+01:00</IntrBkSttlmDt>
            <RtrdInstdAmt Ccy=""EUR"">100.</RtrdInstdAmt>
            <ChrgBr>SHAR</ChrgBr>
            <InstgAgt>
                <FinInstnId>
                    <BICFI>AABBGB2X</BICFI>
                </FinInstnId>
            </InstgAgt>
            <InstdAgt>
                <FinInstnId>
                    <BICFI>AABBIE20</BICFI>
                </FinInstnId>
            </InstdAgt>
            <RtrChain>
                <Dbtr>
                    <Pty>
                        <Nm>{0}</Nm>
                    </Pty>
                </Dbtr>
                <Cdtr>
                    <Pty>
                        <Nm>{1}</Nm>
                    </Pty>
                </Cdtr>
            </RtrChain>
            <RtrRsnInf>
                <Rsn>
                    <Cd>AC04</Cd>
                </Rsn>
            </RtrRsnInf>
        </TxInf>
    </PmtRtr>
</Document>
";
            if (TextBox7.Text.Length != 31 && TextBox7.Text.Substring(0, 4) != TextBox3.Text)
            {
                TextBox7.Text = TextBox3.Text.ToString().Trim() + TextBox7.Text;
            }

            TextBox9.Text = string.Format(str_input, txt_name1.Text.Trim(), txt_name2.Text.Trim());


            objRes = obj.OSB_Scan(TextBox2.Text, TextBox3.Text, TextBox4.Text, TextBox5.Text, TextBox6.Text, TextBox7.Text,
                TextBox8.Text, TextBox9.Text);

            L_ReturnCode.Text = objRes.RESULTCODE;
            L_ReturnMessage.Text = objRes.RESULTMESSAGE;
        }
    }
}