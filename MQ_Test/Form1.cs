using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Threading;
using IBM.WMQ;

namespace MQWorks
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class Form1 : System.Windows.Forms.Form
    {
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_PRGBR_QMGR;
        private System.Windows.Forms.TextBox tb_PRGBR_REQUESTQ;
        private System.Windows.Forms.TextBox tb_PRGBR_RESPONSEQ;
        private System.Windows.Forms.TextBox tb_PRGBR_REQUEST_TEXT;
        private System.Windows.Forms.TextBox tb_PRGBR_RESPONSE_TEXT;
        private System.Windows.Forms.Button btn_PRGBR_PUT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nud_PRGBR_WaitMS;
        private System.Windows.Forms.CheckBox cb_PRGBR_Wait_Infinite;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cb_GRPBR_Wait_Infinite;
        private System.Windows.Forms.NumericUpDown nud_GRPBR_WaitMS;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_GRPBR_REQUEST_TEXT;
        private System.Windows.Forms.TextBox tb_GRPBR_QMGR;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb_GRPBR_REQUESTQ;
        private System.Windows.Forms.Button btn_GRPBR_GET;
        private System.Windows.Forms.TextBox tb_GRPBR_RESPONSE_TEXT;
        private System.Windows.Forms.Button btn_GRPBR_ABORTWAIT;

        private Thread tGetRequestThread;
        private System.Windows.Forms.Label label11;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public Form1()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cb_PRGBR_Wait_Infinite = new System.Windows.Forms.CheckBox();
            this.nud_PRGBR_WaitMS = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_PRGBR_PUT = new System.Windows.Forms.Button();
            this.tb_PRGBR_RESPONSE_TEXT = new System.Windows.Forms.TextBox();
            this.tb_PRGBR_REQUEST_TEXT = new System.Windows.Forms.TextBox();
            this.tb_PRGBR_QMGR = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_PRGBR_REQUESTQ = new System.Windows.Forms.TextBox();
            this.tb_PRGBR_RESPONSEQ = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cb_GRPBR_Wait_Infinite = new System.Windows.Forms.CheckBox();
            this.nud_GRPBR_WaitMS = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_GRPBR_GET = new System.Windows.Forms.Button();
            this.tb_GRPBR_RESPONSE_TEXT = new System.Windows.Forms.TextBox();
            this.tb_GRPBR_REQUEST_TEXT = new System.Windows.Forms.TextBox();
            this.tb_GRPBR_QMGR = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_GRPBR_REQUESTQ = new System.Windows.Forms.TextBox();
            this.btn_GRPBR_ABORTWAIT = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_PRGBR_WaitMS)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_GRPBR_WaitMS)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cb_PRGBR_Wait_Infinite);
            this.groupBox1.Controls.Add(this.nud_PRGBR_WaitMS);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btn_PRGBR_PUT);
            this.groupBox1.Controls.Add(this.tb_PRGBR_RESPONSE_TEXT);
            this.groupBox1.Controls.Add(this.tb_PRGBR_REQUEST_TEXT);
            this.groupBox1.Controls.Add(this.tb_PRGBR_QMGR);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tb_PRGBR_REQUESTQ);
            this.groupBox1.Controls.Add(this.tb_PRGBR_RESPONSEQ);
            this.groupBox1.Location = new System.Drawing.Point(8, 176);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(632, 176);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Put Request Get Back Reply";
            // 
            // cb_PRGBR_Wait_Infinite
            // 
            this.cb_PRGBR_Wait_Infinite.Location = new System.Drawing.Point(328, 119);
            this.cb_PRGBR_Wait_Infinite.Name = "cb_PRGBR_Wait_Infinite";
            this.cb_PRGBR_Wait_Infinite.Size = new System.Drawing.Size(216, 20);
            this.cb_PRGBR_Wait_Infinite.TabIndex = 11;
            this.cb_PRGBR_Wait_Infinite.Text = "Wait Infinitely (May hang if checked)";
            this.cb_PRGBR_Wait_Infinite.CheckedChanged += new System.EventHandler(this.cb_PRGBR_Wait_Infinite_CheckedChanged);
            // 
            // nud_PRGBR_WaitMS
            // 
            this.nud_PRGBR_WaitMS.Increment = new System.Decimal(new int[] {
                                                                               20,
                                                                               0,
                                                                               0,
                                                                               0});
            this.nud_PRGBR_WaitMS.Location = new System.Drawing.Point(264, 119);
            this.nud_PRGBR_WaitMS.Maximum = new System.Decimal(new int[] {
                                                                             10000,
                                                                             0,
                                                                             0,
                                                                             0});
            this.nud_PRGBR_WaitMS.Name = "nud_PRGBR_WaitMS";
            this.nud_PRGBR_WaitMS.ReadOnly = true;
            this.nud_PRGBR_WaitMS.Size = new System.Drawing.Size(56, 20);
            this.nud_PRGBR_WaitMS.TabIndex = 12;
            this.nud_PRGBR_WaitMS.Value = new System.Decimal(new int[] {
                                                                           3000,
                                                                           0,
                                                                           0,
                                                                           0});
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(16, 119);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 16);
            this.label6.TabIndex = 11;
            this.label6.Text = "Milliseconds to wait on GET:";
            // 
            // btn_PRGBR_PUT
            // 
            this.btn_PRGBR_PUT.BackColor = System.Drawing.Color.Bisque;
            this.btn_PRGBR_PUT.ForeColor = System.Drawing.Color.Crimson;
            this.btn_PRGBR_PUT.Location = new System.Drawing.Point(568, 120);
            this.btn_PRGBR_PUT.Name = "btn_PRGBR_PUT";
            this.btn_PRGBR_PUT.Size = new System.Drawing.Size(48, 20);
            this.btn_PRGBR_PUT.TabIndex = 12;
            this.btn_PRGBR_PUT.Text = "&Put";
            this.btn_PRGBR_PUT.Click += new System.EventHandler(this.btn_PRGBR_PUT_Click);
            // 
            // tb_PRGBR_RESPONSE_TEXT
            // 
            this.tb_PRGBR_RESPONSE_TEXT.Location = new System.Drawing.Point(264, 144);
            this.tb_PRGBR_RESPONSE_TEXT.Name = "tb_PRGBR_RESPONSE_TEXT";
            this.tb_PRGBR_RESPONSE_TEXT.ReadOnly = true;
            this.tb_PRGBR_RESPONSE_TEXT.Size = new System.Drawing.Size(352, 20);
            this.tb_PRGBR_RESPONSE_TEXT.TabIndex = 9;
            this.tb_PRGBR_RESPONSE_TEXT.Text = "";
            // 
            // tb_PRGBR_REQUEST_TEXT
            // 
            this.tb_PRGBR_REQUEST_TEXT.Location = new System.Drawing.Point(264, 95);
            this.tb_PRGBR_REQUEST_TEXT.Name = "tb_PRGBR_REQUEST_TEXT";
            this.tb_PRGBR_REQUEST_TEXT.Size = new System.Drawing.Size(352, 20);
            this.tb_PRGBR_REQUEST_TEXT.TabIndex = 10;
            this.tb_PRGBR_REQUEST_TEXT.Text = "This is a test message";
            // 
            // tb_PRGBR_QMGR
            // 
            this.tb_PRGBR_QMGR.Location = new System.Drawing.Point(264, 23);
            this.tb_PRGBR_QMGR.Name = "tb_PRGBR_QMGR";
            this.tb_PRGBR_QMGR.Size = new System.Drawing.Size(352, 20);
            this.tb_PRGBR_QMGR.TabIndex = 7;
            this.tb_PRGBR_QMGR.Text = "QM_w4545281";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(16, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "REQUEST TEXT to PUT:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(248, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "REQUEST Queue Name for PUTTING Request:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Queue Manager Name:";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(248, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "RESPONSE Queue Name for GETTING Reply:";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(16, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "RESPONSE TEXT GOT:";
            // 
            // tb_PRGBR_REQUESTQ
            // 
            this.tb_PRGBR_REQUESTQ.Location = new System.Drawing.Point(264, 48);
            this.tb_PRGBR_REQUESTQ.Name = "tb_PRGBR_REQUESTQ";
            this.tb_PRGBR_REQUESTQ.Size = new System.Drawing.Size(352, 20);
            this.tb_PRGBR_REQUESTQ.TabIndex = 8;
            this.tb_PRGBR_REQUESTQ.Text = "request_q";
            // 
            // tb_PRGBR_RESPONSEQ
            // 
            this.tb_PRGBR_RESPONSEQ.Location = new System.Drawing.Point(264, 72);
            this.tb_PRGBR_RESPONSEQ.Name = "tb_PRGBR_RESPONSEQ";
            this.tb_PRGBR_RESPONSEQ.Size = new System.Drawing.Size(352, 20);
            this.tb_PRGBR_RESPONSEQ.TabIndex = 9;
            this.tb_PRGBR_RESPONSEQ.Text = "response_q";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cb_GRPBR_Wait_Infinite);
            this.groupBox2.Controls.Add(this.nud_GRPBR_WaitMS);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.btn_GRPBR_GET);
            this.groupBox2.Controls.Add(this.tb_GRPBR_RESPONSE_TEXT);
            this.groupBox2.Controls.Add(this.tb_GRPBR_REQUEST_TEXT);
            this.groupBox2.Controls.Add(this.tb_GRPBR_QMGR);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.tb_GRPBR_REQUESTQ);
            this.groupBox2.Controls.Add(this.btn_GRPBR_ABORTWAIT);
            this.groupBox2.Location = new System.Drawing.Point(8, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(632, 152);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Get Request Put Back Reply";
            // 
            // cb_GRPBR_Wait_Infinite
            // 
            this.cb_GRPBR_Wait_Infinite.Checked = true;
            this.cb_GRPBR_Wait_Infinite.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_GRPBR_Wait_Infinite.Location = new System.Drawing.Point(328, 96);
            this.cb_GRPBR_Wait_Infinite.Name = "cb_GRPBR_Wait_Infinite";
            this.cb_GRPBR_Wait_Infinite.Size = new System.Drawing.Size(104, 20);
            this.cb_GRPBR_Wait_Infinite.TabIndex = 4;
            this.cb_GRPBR_Wait_Infinite.Text = "Wait Infinitely";
            this.cb_GRPBR_Wait_Infinite.CheckedChanged += new System.EventHandler(this.cb_GRPBR_Wait_Infinite_CheckedChanged);
            // 
            // nud_GRPBR_WaitMS
            // 
            this.nud_GRPBR_WaitMS.Enabled = false;
            this.nud_GRPBR_WaitMS.Increment = new System.Decimal(new int[] {
                                                                               20,
                                                                               0,
                                                                               0,
                                                                               0});
            this.nud_GRPBR_WaitMS.Location = new System.Drawing.Point(264, 96);
            this.nud_GRPBR_WaitMS.Maximum = new System.Decimal(new int[] {
                                                                             10000,
                                                                             0,
                                                                             0,
                                                                             0});
            this.nud_GRPBR_WaitMS.Name = "nud_GRPBR_WaitMS";
            this.nud_GRPBR_WaitMS.ReadOnly = true;
            this.nud_GRPBR_WaitMS.Size = new System.Drawing.Size(56, 20);
            this.nud_GRPBR_WaitMS.TabIndex = 12;
            this.nud_GRPBR_WaitMS.Value = new System.Decimal(new int[] {
                                                                           10000,
                                                                           0,
                                                                           0,
                                                                           0});
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(16, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(152, 16);
            this.label7.TabIndex = 11;
            this.label7.Text = "Milliseconds to wait on GET:";
            // 
            // btn_GRPBR_GET
            // 
            this.btn_GRPBR_GET.BackColor = System.Drawing.Color.Bisque;
            this.btn_GRPBR_GET.ForeColor = System.Drawing.Color.Crimson;
            this.btn_GRPBR_GET.Location = new System.Drawing.Point(480, 96);
            this.btn_GRPBR_GET.Name = "btn_GRPBR_GET";
            this.btn_GRPBR_GET.Size = new System.Drawing.Size(48, 20);
            this.btn_GRPBR_GET.TabIndex = 5;
            this.btn_GRPBR_GET.Text = "&Get";
            this.btn_GRPBR_GET.Click += new System.EventHandler(this.btn_GRPBR_GET_Click);
            // 
            // tb_GRPBR_RESPONSE_TEXT
            // 
            this.tb_GRPBR_RESPONSE_TEXT.Location = new System.Drawing.Point(264, 72);
            this.tb_GRPBR_RESPONSE_TEXT.Name = "tb_GRPBR_RESPONSE_TEXT";
            this.tb_GRPBR_RESPONSE_TEXT.Size = new System.Drawing.Size(352, 20);
            this.tb_GRPBR_RESPONSE_TEXT.TabIndex = 3;
            this.tb_GRPBR_RESPONSE_TEXT.Text = "Message received, thank you!";
            // 
            // tb_GRPBR_REQUEST_TEXT
            // 
            this.tb_GRPBR_REQUEST_TEXT.Location = new System.Drawing.Point(264, 120);
            this.tb_GRPBR_REQUEST_TEXT.Name = "tb_GRPBR_REQUEST_TEXT";
            this.tb_GRPBR_REQUEST_TEXT.ReadOnly = true;
            this.tb_GRPBR_REQUEST_TEXT.Size = new System.Drawing.Size(352, 20);
            this.tb_GRPBR_REQUEST_TEXT.TabIndex = 8;
            this.tb_GRPBR_REQUEST_TEXT.Text = "";
            // 
            // tb_GRPBR_QMGR
            // 
            this.tb_GRPBR_QMGR.Location = new System.Drawing.Point(264, 23);
            this.tb_GRPBR_QMGR.Name = "tb_GRPBR_QMGR";
            this.tb_GRPBR_QMGR.Size = new System.Drawing.Size(352, 20);
            this.tb_GRPBR_QMGR.TabIndex = 1;
            this.tb_GRPBR_QMGR.Text = "QM_w4545281";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(16, 120);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 16);
            this.label8.TabIndex = 3;
            this.label8.Text = "REQUEST TEXT GOT:";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(16, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(248, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "REQUEST Queue Name for GETTING Request:";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(16, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(128, 16);
            this.label10.TabIndex = 0;
            this.label10.Text = "Queue Manager Name:";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(16, 72);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(248, 16);
            this.label12.TabIndex = 4;
            this.label12.Text = "RESPONSE TEXT to PUT if Request Received:";
            // 
            // tb_GRPBR_REQUESTQ
            // 
            this.tb_GRPBR_REQUESTQ.Location = new System.Drawing.Point(264, 48);
            this.tb_GRPBR_REQUESTQ.Name = "tb_GRPBR_REQUESTQ";
            this.tb_GRPBR_REQUESTQ.Size = new System.Drawing.Size(352, 20);
            this.tb_GRPBR_REQUESTQ.TabIndex = 2;
            this.tb_GRPBR_REQUESTQ.Text = "request_q";
            // 
            // btn_GRPBR_ABORTWAIT
            // 
            this.btn_GRPBR_ABORTWAIT.BackColor = System.Drawing.Color.Bisque;
            this.btn_GRPBR_ABORTWAIT.ForeColor = System.Drawing.Color.Crimson;
            this.btn_GRPBR_ABORTWAIT.Location = new System.Drawing.Point(536, 96);
            this.btn_GRPBR_ABORTWAIT.Name = "btn_GRPBR_ABORTWAIT";
            this.btn_GRPBR_ABORTWAIT.Size = new System.Drawing.Size(80, 20);
            this.btn_GRPBR_ABORTWAIT.TabIndex = 6;
            this.btn_GRPBR_ABORTWAIT.Text = "&Abort Wait";
            this.btn_GRPBR_ABORTWAIT.Click += new System.EventHandler(this.btn_GRPBR_ABORTWAIT_Click);
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(8, 360);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(632, 64);
            this.label11.TabIndex = 2;
            this.label11.Text = @"For proper demonstration: (a) Simple: Change the Queue Manager Name and Queue Names to your own. Click on PUT, and after successful PUT, click CANCEL on the message box. Then click GET to retireve the message. (b) More Complex: Change the Queue Manager Name and Queue Names to your own.  Then, with the top ""wait Infinitely"" checked, click on GET. A thread will be spawned that will wait for somebody to put a request message in the request Queue. Then, with the bottom ""Wait Infintely"" unchecked, click PUT. See what happens.";
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(648, 428);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "MQ Works With C# - by Koushik Biswas. THANKS: John Concannon";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nud_PRGBR_WaitMS)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nud_GRPBR_WaitMS)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new Form1());
        }

        private void Form1_Load(object sender, System.EventArgs e)
        {
            cb_PRGBR_Wait_Infinite.CheckState = CheckState.Unchecked;
            cb_GRPBR_Wait_Infinite.CheckState = CheckState.Checked;
            nud_GRPBR_WaitMS.Enabled = false;
            btn_GRPBR_ABORTWAIT.Enabled = false;
            tGetRequestThread = null;
            tb_GRPBR_QMGR.SelectAll();
        }

        private void btn_PRGBR_PUT_Click(object sender, System.EventArgs e)
        {
            tb_PRGBR_RESPONSE_TEXT.Text = "";

            MQQueueManager mqQMgr;
            MQQueue requestQueue;
            MQQueue responseQueue;
            MQRCText mqrcText = new MQRCText();
            MQMessage requestMessage;
            MQMessage responseMessage;

            string strRequestQueueName = tb_PRGBR_REQUESTQ.Text.Trim();
            string strResponseQueueName = tb_PRGBR_RESPONSEQ.Text.Trim();
            string strQueueManagerName = tb_PRGBR_QMGR.Text.Trim();
            string strRequestText = tb_PRGBR_REQUEST_TEXT.Text.Trim();

            if (strQueueManagerName == "")
            {
                MessageBox.Show("Please enter a Queue Manager Name");
                tb_PRGBR_QMGR.Focus();
                return;
            }

            if (strRequestQueueName == "")
            {
                MessageBox.Show("Please enter a Request Queue Name");
                tb_PRGBR_REQUESTQ.Focus();
                return;
            }

            if (strResponseQueueName == "")
            {
                MessageBox.Show("Please enter a Response Queue Name");
                tb_PRGBR_RESPONSEQ.Focus();
                return;
            }

            if (strRequestText == "")
            {
                MessageBox.Show("Please enter a Request Text");
                tb_PRGBR_REQUEST_TEXT.Focus();
                return;
            }

            //Step 1. Create Queue Manager Object. This will also CONNECT the Queue Manager
            try
            {
                mqQMgr = new MQQueueManager(strQueueManagerName);
            }
            catch (MQException mqe)
            {
                string strError = mqrcText.getMQRCText(mqe.Reason);
                MessageBox.Show("Error trying to create Queue Manager Object. Error: " + mqe.Message + ", Details: " + strError);
                return;
            }

            //Step 2. Open Request Queue for writing our request
            try
            {
                requestQueue = mqQMgr.AccessQueue(strRequestQueueName,
                    MQC.MQOO_OUTPUT                     // open queue for output
                    + MQC.MQOO_FAIL_IF_QUIESCING);      // but not if MQM stopping
            }
            catch (MQException mqe)
            {
                string strError = mqrcText.getMQRCText(mqe.Reason);
                MessageBox.Show("Error trying to open Request Queue for writing. Error: " + mqe.Message + ", Details: " + strError);
                if (mqQMgr.ConnectionStatus)
                    mqQMgr.Disconnect();
                return;
            }

            //Step 3. Open Response Queue for reading the response.
            //Note: You can do this AFTER writing the request too. Depends on the protocol
            //you are using - may be you are not supposed to write the request unless you
            //ensure that you are listening for response too. If that is not the case,
            //you can do this after writing the request message (before step 5).
            try
            {
                responseQueue = mqQMgr.AccessQueue(strResponseQueueName,
                    MQC.MQOO_INPUT_AS_Q_DEF             // open queue for input
                    + MQC.MQOO_FAIL_IF_QUIESCING);      // but not if MQM stopping
            }
            catch (MQException mqe)
            {
                string strError = mqrcText.getMQRCText(mqe.Reason);
                MessageBox.Show("Error trying to open Response Queue for reading. Error: " + mqe.Message + ", Details: " + strError);
                if (requestQueue.OpenStatus)
                    requestQueue.Close();
                if (mqQMgr.ConnectionStatus)
                    mqQMgr.Disconnect();
                return;
            }

            //Step 4. PUT Request Message in Request Queue. Note the options needed to be set.
            //Note that once PUT is successful, you can close the Request Queue. Note that you are
            //asking whoever receives this request to copy the MSG ID to CORREL ID so that
            //you can later "match" the response you get with the request you sent.
            try
            {
                requestMessage = new MQMessage();
                requestMessage.WriteString(strRequestText);
                requestMessage.Format = MQC.MQFMT_STRING;
                requestMessage.MessageType = MQC.MQMT_REQUEST;
                requestMessage.Report = MQC.MQRO_COPY_MSG_ID_TO_CORREL_ID;
                requestMessage.ReplyToQueueName = strResponseQueueName;
                requestMessage.ReplyToQueueManagerName = strQueueManagerName;
                requestQueue.Put(requestMessage);
                if (requestQueue.OpenStatus)
                    requestQueue.Close();
                if (MessageBox.Show("Request Message PUT successfully. You can verify in MQ Explorer now.\nClick OK to start waiting for RESPONSE, Cancel to skip waiting", "Do you want to wait for response?", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                {
                    if (responseQueue.OpenStatus)
                        responseQueue.Close();
                    if (mqQMgr.ConnectionStatus)
                        mqQMgr.Disconnect();
                    return;
                }
            }
            catch (MQException mqe)
            {
                string strError = mqrcText.getMQRCText(mqe.Reason);
                MessageBox.Show("Error trying to PUT Message to Request Queue. Error: " + mqe.Message + ", Details: " + strError);
                if (requestQueue.OpenStatus)
                    requestQueue.Close();
                if (responseQueue.OpenStatus)
                    responseQueue.Close();
                if (mqQMgr.ConnectionStatus)
                    mqQMgr.Disconnect();
                return;
            }

            //Step 5. Read the response from response queue. Note the options to be set.
            //It may happen that you get no response. Also note that you decide how long you wait
            //for the response. In order to get the response, note the "matching" criterion.
            try
            {
                responseMessage = new MQMessage();
                responseMessage.CorrelationId = requestMessage.MessageId;
                MQGetMessageOptions gmo = new MQGetMessageOptions();
                gmo.Options = MQC.MQGMO_WAIT;
                if (nud_PRGBR_WaitMS.Enabled == true)
                    gmo.WaitInterval = (int)nud_PRGBR_WaitMS.Value;
                else
                    gmo.WaitInterval = MQC.MQWI_UNLIMITED;
                gmo.MatchOptions = MQC.MQMO_MATCH_CORREL_ID;

                responseQueue.Get(responseMessage, gmo);
                tb_PRGBR_RESPONSE_TEXT.Text = responseMessage.ReadString(responseMessage.MessageLength);
            }
            catch (MQException mqe)
            {
                string strError = mqrcText.getMQRCText(mqe.Reason);
                if (strError.Equals("MQRC_NO_MSG_AVAILABLE"))
                    MessageBox.Show("No Message found in Response Queue after waiting for " + nud_PRGBR_WaitMS.Value.ToString() + " MilliSeconds");
                else
                    MessageBox.Show("Error trying to GET Message from Response Queue. Error: " + mqe.Message + ", Details: " + strError);
                if (responseQueue.OpenStatus)
                    responseQueue.Close();
                if (mqQMgr.ConnectionStatus)
                    mqQMgr.Disconnect();
                return;
            }

            //Step 6. Close Response Queue, Disconnect Manager. Note that you have already
            //closed the request queue in step 4.
            if (responseQueue.OpenStatus)
                responseQueue.Close();
            if (mqQMgr.ConnectionStatus)
                mqQMgr.Disconnect();
            return;
        }

        private void cb_PRGBR_Wait_Infinite_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cb_PRGBR_Wait_Infinite.CheckState == CheckState.Checked)
                nud_PRGBR_WaitMS.Enabled = false;
            else
                nud_PRGBR_WaitMS.Enabled = true;
        }

        private void cb_GRPBR_Wait_Infinite_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cb_GRPBR_Wait_Infinite.CheckState == CheckState.Checked)
                nud_GRPBR_WaitMS.Enabled = false;
            else
                nud_GRPBR_WaitMS.Enabled = true;
        }

        private void btn_GRPBR_GET_Click(object sender, System.EventArgs e)
        {
            tb_GRPBR_REQUEST_TEXT.Text = "";
            tGetRequestThread = new Thread(new ThreadStart(GRPBRGetClickThread));
            tGetRequestThread.Start();
            btn_GRPBR_ABORTWAIT.Enabled = true;
        }

        private void GRPBRGetClickThread()
        {
            MQQueueManager mqQMgr;
            MQQueue requestQueue;
            MQQueue responseQueue;
            MQRCText mqrcText = new MQRCText();
            MQMessage requestMessage;
            MQMessage responseMessage;

            string strRequestQueueName = tb_GRPBR_REQUESTQ.Text.Trim();
            string strQueueManagerName = tb_GRPBR_QMGR.Text.Trim();
            string strResponseText = tb_GRPBR_RESPONSE_TEXT.Text.Trim();

            if (strQueueManagerName == "")
            {
                MessageBox.Show("Please enter a Queue Manager Name");
                tb_GRPBR_QMGR.Focus();
                btn_GRPBR_ABORTWAIT.Enabled = false;
                return;
            }

            if (strRequestQueueName == "")
            {
                MessageBox.Show("Please enter a Request Queue Name");
                tb_GRPBR_REQUESTQ.Focus();
                btn_GRPBR_ABORTWAIT.Enabled = false;
                return;
            }

            if (strResponseText == "")
            {
                MessageBox.Show("Please enter a Response Text");
                tb_GRPBR_RESPONSE_TEXT.Focus();
                btn_GRPBR_ABORTWAIT.Enabled = false;
                return;
            }

            //Step 1. Create Queue Manager Object. This will also CONNECT the Queue Manager
            try
            {
                mqQMgr = new MQQueueManager(strQueueManagerName);
            }
            catch (MQException mqe)
            {
                string strError = mqrcText.getMQRCText(mqe.Reason);
                MessageBox.Show("Error trying to create Queue Manager Object. Error: " + mqe.Message + ", Details: " + strError);
                btn_GRPBR_ABORTWAIT.Enabled = false;
                return;
            }

            //Step 2. Open Request Queue for reading/ getting the request
            try
            {
                requestQueue = mqQMgr.AccessQueue(strRequestQueueName,
                    MQC.MQOO_INPUT_AS_Q_DEF             // open queue for input
                    + MQC.MQOO_FAIL_IF_QUIESCING);      // but not if MQM stopping
            }
            catch (MQException mqe)
            {
                string strError = mqrcText.getMQRCText(mqe.Reason);
                MessageBox.Show("Error trying to open Request Queue for reading. Error: " + mqe.Message + ", Details: " + strError);
                if (mqQMgr.ConnectionStatus)
                    mqQMgr.Disconnect();
                btn_GRPBR_ABORTWAIT.Enabled = false;
                return;
            }

            //Step 3. GET the request message. Note that you decide how long you wait for a message
            //to show up. As you are the server now, you do no matching - you must serve to all
            //clients. Guess why this method needed a separate thread? Because the GET() can hang the
            //thread if it is long enough. Note that request queue is NOT closed after GET is over.
            try
            {
                requestMessage = new MQMessage();
                MQGetMessageOptions gmo = new MQGetMessageOptions();
                gmo.Options = MQC.MQGMO_WAIT;                 // must be specified if you use wait interval
                if (nud_GRPBR_WaitMS.Enabled == false)
                    gmo.WaitInterval = MQC.MQWI_UNLIMITED;        // wait forever
                else
                    gmo.WaitInterval = (int)nud_GRPBR_WaitMS.Value;
                gmo.MatchOptions = MQC.MQMO_NONE;             // no matching required
                requestQueue.Get(requestMessage, gmo);
                tb_GRPBR_REQUEST_TEXT.Text = requestMessage.ReadString(requestMessage.MessageLength);
            }
            catch (MQException mqe)
            {
                string strError = mqrcText.getMQRCText(mqe.Reason);
                MessageBox.Show("Error trying to GET message from Request Queue. Error: " + mqe.Message + ", Details: " + strError);
                if (requestQueue.OpenStatus)
                    requestQueue.Close();
                if (mqQMgr.ConnectionStatus)
                    mqQMgr.Disconnect();
                btn_GRPBR_ABORTWAIT.Enabled = false;
                return;
            }

            //Step 4. Open Response Queue to write the reply message. Note that we retrieve the
            //"reply to" queue name and manager name from the request message itself. That's why
            //we do not need the Response Queue name from the user in our GUI in this case.
            try
            {
                responseQueue = mqQMgr.AccessQueue(requestMessage.ReplyToQueueName,
                    MQC.MQOO_OUTPUT, requestMessage.ReplyToQueueManagerName, null, null);
            }
            catch (MQException mqe)
            {
                string strError = mqrcText.getMQRCText(mqe.Reason);
                MessageBox.Show("Error trying to open queue for replying. Error: " + mqe.Message + ", Details: " + strError);
                if (requestQueue.OpenStatus)
                    requestQueue.Close();
                if (mqQMgr.ConnectionStatus)
                    mqQMgr.Disconnect();
                btn_GRPBR_ABORTWAIT.Enabled = false;
                return;
            }

            //Step 5. Reply back (PUT the response message) Note the response "matching"
            //mechanism to ensure that when the sender receives the correct ID in the
            //correct place (as requested by sender), so that it can "match" the response.

            //Step 6. Close Request, Response Queues and Disconnect Manager.
            try
            {
                responseMessage = new MQMessage();
                MQPutMessageOptions pmo = new MQPutMessageOptions();
                pmo.Options = MQC.MQPMO_NONE;

                if ((requestMessage.Report & MQC.MQRO_PASS_MSG_ID) == MQC.MQRO_PASS_MSG_ID)
                    responseMessage.MessageId = requestMessage.CorrelationId;
                else                        // Assume MQRO_NEW_MSG_ID
                    pmo.Options = MQC.MQPMO_NEW_MSG_ID;

                if ((requestMessage.Report & MQC.MQRO_PASS_CORREL_ID) == MQC.MQRO_PASS_CORREL_ID)
                    responseMessage.CorrelationId = requestMessage.CorrelationId;
                else                        // Assume MQRO_COPY_MSG_ID_TO_CORREL_ID
                    responseMessage.CorrelationId = requestMessage.MessageId;

                responseMessage.MessageType = MQC.MQMT_REPLY;
                responseMessage.WriteString(strResponseText);
                responseQueue.Put(responseMessage);
                if (responseQueue.OpenStatus)
                    responseQueue.Close();
                if (requestQueue.OpenStatus)
                    requestQueue.Close();
                if (mqQMgr.ConnectionStatus)
                    mqQMgr.Disconnect();
                btn_GRPBR_ABORTWAIT.Enabled = false;
                return;
            }
            catch (MQException mqe)
            {
                string strError = mqrcText.getMQRCText(mqe.Reason);
                MessageBox.Show("Error trying to send Reply. Error: " + mqe.Message + ", Details: " + strError);
                if (responseQueue.OpenStatus)
                    responseQueue.Close();
                if (requestQueue.OpenStatus)
                    requestQueue.Close();
                if (mqQMgr.ConnectionStatus)
                    mqQMgr.Disconnect();
                btn_GRPBR_ABORTWAIT.Enabled = false;
                return;
            }
        }

        private void btn_GRPBR_ABORTWAIT_Click(object sender, System.EventArgs e)
        {
            tGetRequestThread.Abort();
        }
    }
}
