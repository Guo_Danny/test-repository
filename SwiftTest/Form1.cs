﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SwiftTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            WinSwiftAmlWS.SwiftAmlWebService swiiftws = new WinSwiftAmlWS.SwiftAmlWebService();

            WinSwiftAmlWS.OSB_QUERYRESULT swiftQR = new WinSwiftAmlWS.OSB_QUERYRESULT();

            string strCurrentTime = "";
            if(textBox3.Text == "Current Time" || textBox3.Text.Trim() == "" || !DateTime.TryParse(textBox3.Text, out DateTime dtime))
            {
                strCurrentTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            }
            else
            {
                strCurrentTime = textBox3.Text;
            }

            //SSL TLS1.2
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return CheckCert();
            };

            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;

            swiftQR = swiiftws.OSB_Query(strCurrentTime, textBox4.Text);

            textBox10.Text = swiftQR.RESULTCODE;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            WinSwiftAmlWS.SwiftAmlWebService swiiftws = new WinSwiftAmlWS.SwiftAmlWebService();
            WinSwiftAmlWS.OSB_SCANRESULT swiftSR = new WinSwiftAmlWS.OSB_SCANRESULT();
            string strCurrentTime = "";
            if (textBox3.Text == "Current Time" || textBox3.Text.Trim() == "" || !DateTime.TryParse(textBox3.Text, out DateTime dtime))
            {
                strCurrentTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            }
            else
            {
                strCurrentTime = textBox3.Text;
            }

            //SSL TLS1.2
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return CheckCert();
            };

            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;
            swiiftws.Timeout = 300 * 1000;
            swiftSR = swiiftws.OSB_Scan(strCurrentTime, textBox4.Text, textBox5.Text,
                textBox6.Text, textBox7.Text, textBox8.Text, textBox9.Text);

            textBox10.Text = swiftSR.RESULTCODE;
        }

        private Boolean CheckCert()
        {
            Boolean bol_r = false;

            if ("a" == "a")
            {
                bol_r = true;
            }

            return bol_r;
        }
    }
}
