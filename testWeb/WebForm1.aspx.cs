﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace testWeb
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private Database database;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                database = EnterpriseLibraryContainer.Current.GetInstance<Database>("PBSA");

                string sql = "";

                sql = "select SourceOfFunds, TypeOfBusiness from PBSA..Customer where id = @id ;";

                using (DbCommand cmd = database.GetSqlStringCommand(sql))
                {
                    cmd.CommandText = sql;

                    //cmd.Parameters.Add(new SqlParameter("@id", Txt_ID.Text.Trim()));
                    database.AddInParameter(cmd, "@id", DbType.String, Txt_ID.Text.Trim());
                    IDataReader reader = database.ExecuteReader(cmd);
                    try
                    {
                        while (reader.Read())
                        {
                            txtSourceOfFunds.Text = reader[0].ToString();
                            txtTypeofBusiness.Text = reader[1].ToString();
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
        }

        protected void btn_save_click(object sender, EventArgs e)
        {
            database = EnterpriseLibraryContainer.Current.GetInstance<Database>("PBSA");

            string sql = "";

            sql = "update Customer set SourceOfFunds = @sourceoffunds, TypeOfBusiness = @typeofbus ";
            sql += "where id = @id ;";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@id", Txt_ID.Text.Trim()));
                cmd.Parameters.Add(new SqlParameter("@sourceoffunds", txtSourceOfFunds.Text.Trim()));
                cmd.Parameters.Add(new SqlParameter("@typeofbus", txtTypeofBusiness.Text.Trim()));

                ExecuteSQL(cmd);
            }

        }

        private int ExecuteSQL(DbCommand cmd)
        {
            using (DbConnection conn = database.CreateConnection())
            {
                conn.Open();
                DbTransaction trans = conn.BeginTransaction();

                try
                {
                    int rows = database.ExecuteNonQuery(cmd, trans);
                    trans.Commit();
                    conn.Close();
                    return rows;
                }
                catch
                {
                    trans.Rollback();
                    conn.Close();
                    throw;
                }
            }
        }

        protected void ddlTPB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}