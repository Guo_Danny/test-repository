﻿using System;
using Google.Authenticator;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Configuration;
using System.Web.Caching;
using System.Web;
using log4net;
using log4net.Config;

namespace testWeb
{
    public partial class MFA_GA : System.Web.UI.Page
    {
        private Database database;
        private String strLoginpage = ConfigurationManager.AppSettings["loginpage"];
        private int icookie_expire = int.Parse(ConfigurationManager.AppSettings["expire"]);
        private String strbranch_no = ConfigurationManager.AppSettings["brno"];
        private static readonly ILog TxtLog = LogManager.GetLogger("App.Logging");

        private static int num;

        public static String AccountSecretKey = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string log4netPath = Server.MapPath("~/log4net.config");
                XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo(log4netPath));

                //Writelog("Log Begin!");
                num = 3;
                timer.Interval = 1000;//設定每秒執行一次
                timer.Enabled = false;//先關閉計時
                if (Request.Cookies["userInfo"] != null)
                {
                    //HttpCookie userInfo = new HttpCookie("userInfo");
                    //userInfo.Expires = DateTime.Now.AddYears(-1);
                    //Response.Cookies.Add(userInfo);
                }
            }
        }

        public void CreateQRCode(string strUserId)
        {
            AccountSecretKey = Base32ToDec(strbranch_no + strUserId);

            //產生QR Code.
            var tfa = new TwoFactorAuthenticator();
            var setupInfo = tfa.GenerateSetupCode(strUserId, strUserId + "@bot.com.tw", AccountSecretKey, 300, 300);

            //用內建的API 產生

            ltlQRCode.Text = "<img src='" + setupInfo.QrCodeSetupImageUrl + "' />";
            ltlQRCodeContent.Text = "otpauth://totp/" + strUserId + "@bot.com.tw" + "?secret=" + setupInfo.ManualEntryKey + "&issuer=" + strUserId;
        }

        protected void btn_pincheck_Click(object sender, EventArgs e)
        {
            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            //第一個參數是你當初產生QRcode 所產生的Secret code 
            //第二個參數是用戶輸入的純數字Code
            var result = tfa.ValidateTwoFactorPIN(AccountSecretKey, txtpin.Text.Trim());
            

            if (result)
            {
                if (Request.Cookies["userInfo"] != null)
                {
                    if (Request.Cookies["userInfo"]["MFAPin"] == txtpin.Text.Trim())
                    {
                        lblResult.Text = "Login fail. Please try again later!";
                    }
                    else
                    {
                        Writelog("[Cookie] UserID=" + txtUserId.Text.Trim() + " Pin Code check success!");

                        lblResult.Text = "SUCCESS!! Now redirecting to login page...";
                        timer.Enabled = true;

                        btn_checkuser.Enabled = false;
                        btn_pincheck.Enabled = false;

                        HttpCookie userInfo = new HttpCookie("userInfo");
                        userInfo["MFACheck"] = txtUserId.Text.Trim(); ;
                        userInfo["MFAPin"] = txtpin.Text.Trim();
                        userInfo.Expires = DateTime.Now.AddMinutes(icookie_expire);
                        Response.Cookies.Add(userInfo);

                    }
                }
                else
                {
                    Writelog("UserID=" + txtUserId.Text.Trim() + " Pin Code check success!");

                    lblResult.Text = "SUCCESS!! Now redirecting to login page...";
                    timer.Enabled = true;

                    btn_checkuser.Enabled = false;
                    btn_pincheck.Enabled = false;

                    HttpCookie userInfo = new HttpCookie("userInfo");
                    userInfo["MFACheck"] = txtUserId.Text.Trim(); ;
                    userInfo["MFAPin"] = txtpin.Text.Trim();
                    userInfo.Expires = DateTime.Now.AddMinutes(icookie_expire);
                    Response.Cookies.Add(userInfo);
                }
            }
            else
            {
                Writelog("UserID=" + txtUserId.Text.Trim() + " Pin Code Check FAIL!");
                lblResult.Text = "Pin Check FAIL!!";
            }
        }

        public String CheckUser()
        {
            database = EnterpriseLibraryContainer.Current.GetInstance<Database>("PSEC");

            string sql = "";
            string str_user = "";
            sql = @"select UserName from PSEC..Operator
                    where Enable = 1
                    and UserName = @id ";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.CommandText = sql;

                //cmd.Parameters.Add(new SqlParameter("@id", Txt_ID.Text.Trim()));
                database.AddInParameter(cmd, "@id", DbType.String, txtUserId.Text.Trim());
                IDataReader reader = database.ExecuteReader(cmd);
                try
                {
                    while (reader.Read())
                    {
                        str_user = reader[0].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }

            return str_user;
        }

        protected void btn_checkuser_Click(object sender, EventArgs e)
        {
            lblResult.Text = "";
            ltlQRCode.Text = "";

            string str_userid = txtUserId.Text.Trim();//CheckUser(); 
            if (str_userid != "")
            {
                Writelog("UserID=" + txtUserId.Text.Trim() + " Exist");
                lblResult.Text = "";
                ltlQRCode.Text = "";
                ltlQRCodeContent.Text = "";

                CreateQRCode(str_userid);
            }
            else
            {
                Writelog("UserID=" + txtUserId.Text.Trim() + " Not Exist!");

                lblResult.Text = "This User do not exist!";
                ltlQRCode.Text = "";
                ltlQRCodeContent.Text = "";
            }
        }

        public static String Base32ToDec(String base32)
        {
            String base32chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
            String bits = "";
            String hex = "";


            for (var i = 0; i < base32.Length; i++)
            {
                int val = base32chars.IndexOf(base32.Substring(i, 1).ToUpper());

                bits += Convert.ToString(val, 2).PadLeft(5, '0');
            }

            for (var i = 0; i + 4 <= bits.Length; i += 4)
            {
                String chunk = bits.Substring(i, 4);
                hex = hex + Convert.ToString(Convert.ToInt32(chunk, 2), 16);
            }
            return hex;
        }

        protected void timer_Tick(object sender, EventArgs e)
        {
            lblResult.Text = "SUCCESS!! Now redirecting to login page..." + num.ToString();
            num--;
            if (num < 0)
            {
                //Response.Redirect(strLoginpage);
                Response.Redirect("http://localhost:59476/MFA_GA.aspx");
            }
        }

        private void Writelog(string strLogMessage)
        {
            Fortify fortifyService = new Fortify();
            TxtLog.Info(fortifyService.PathManipulation(strLogMessage));
        }
    }
}