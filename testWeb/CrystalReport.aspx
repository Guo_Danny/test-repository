<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CrystalReport.aspx.vb" Inherits="CMBrowser.CrystalReport" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>




<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CrystalReport</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" language="JavaScript" src="js/nav.js"></script>
		<script language="javascript" id="clientEventHandlersJS">
<!--

function loadreport() {

	try
	{		

		if (document.forms[0].item("hfldRequest").value=="1")
		{
			
			document.forms[0].item("hfldRequest").value="2";
			__doPostBack("lblWait", "");
		}
		else if (document.forms[0].item("hfldRequest").value=="3")
		{
			window.close();
		}
		else
		{
			document.getElementById("wait").style.visibility="hidden";
			
		}
	}
    catch(ex){}


}

function closereport()
{
 try
 {
	document.forms[0].item("hfldRequest").value = "3";
	__doPostBack("lblClose", "");
 }
 catch(ex){}
}


function Form1_onsubmit()
{
	try
	{
		return showwait();
	}
	catch(ex){}
}

function document_onclick()
{
	try
	{

		if (event.srcElement.value=="Submit")
			showwait();
		
	}
	catch(ex){}
	finally
	{
		return true;
	}
}

function window_onunload() 
{
	try
	{
		return showwait();
	}
	catch(ex){}
}

function showwait() 
{
	try
	{
		document.getElementById("wait").style.visibility="visible";
		return true;
	}
	catch(ex){}
}

function cmdClose_onclick() {
window.close();
}

//-->
		</script>
		<script language="javascript" event="onclick" for="document">
	<!--

		return document_onclick();
		//-->
		</script>
	</HEAD>
	<body language="javascript" class="formbody" onload="loadreport();AddWindowToPCSWindowCookie(window.name);" onunload="RemoveWindowFromPCSWindowCookie(window.name);return window_onunload()" >
		<form language="javascript" id="Form1" onsubmit="return Form1_onsubmit()" method="post"
			runat="server">
			<table id="wait" style="LEFT: 650px; WIDTH: 200px; POSITION: absolute; TOP: 15px; HEIGHT: 50px; BACKGROUND-COLOR: transparent">
				<tr>
					<td align="center"><asp:label id="lblWait" runat="server" CssClass="RecordMessage">
						Please Wait...
					</asp:label><INPUT id="hfldRequest" type="hidden" value="1" name="hfldRequest" runat="server">
					</td>
				</tr>
			</table>
			<table id="close" style="LEFT: 850px; WIDTH: 200px; POSITION: absolute; TOP: 15px; HEIGHT: 50px; BACKGROUND-COLOR: transparent">
				<tr>
					<td valign="top" align="center" Onclick="closereport();">
						<asp:label id="lblClose" runat="server" style="FONT-WEIGHT: bold; FONT-SIZE: x-small; CURSOR: hand; COLOR: blue; TEXT-DECORATION: none"
							Width="75">
						Close
						</asp:label>
					</td>
				</tr>
			</table>
			<table id="PageLayout" width="905">
				<tr>
					<td width="5">&nbsp;</td>
					<td width="900"><asp:imagebutton id="imgPrime" style="OVERFLOW: hidden" runat="server" AlternateText="Home" ImageUrl="images/compliance-manager-2.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td></td>
					<td vAlign="top"><asp:label id="lblMessage" runat="server"></asp:label></td>
				</tr>
				<tr>
					<td></td>
					<td vAlign="top"><asp:panel id="pnlOFACSncInExclParameters" runat="server">
							<TABLE cellSpacing="2" cellPadding="2" width="650">
								<TR>
									<TD align="center" width="250">
										<asp:Label id="Label1" runat="server" CssClass="LABEL">Exclude Names</asp:Label></TD>
									<TD width="50">&nbsp;</TD>
									<TD align="center" width="350">
										<asp:Label id="Label2" runat="server" CssClass="LABEL">Matching Rules</asp:Label></TD>
								</TR>
								<TR>
									<TD>
										<asp:CheckBox id="chkTransExclNames" runat="server" CssClass="LABEL" Checked="True" Text="Transactional Exclude Names"></asp:CheckBox></TD>
									<TD></TD>
									<TD>
										<asp:RadioButton id="optMatchFullName" runat="server" CssClass="OPTLABEL" Text="Match Full Name"
											GroupName="grpAlgorithm"></asp:RadioButton>&nbsp;
										<asp:RadioButton id="optMatchText" runat="server" CssClass="OPTLABEL" Checked="True" Text="Match Text"
											GroupName="grpAlgorithm"></asp:RadioButton></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" align="left">
										<asp:CheckBox id="chkCIFExcludeNames" runat="server" CssClass="LABEL" Checked="True" Text="CIF Exclude Names"></asp:CheckBox></TD>
									<TD style="HEIGHT: 25px"></TD>
									<TD style="HEIGHT: 25px" align="left">
										<asp:Label id="lblFileImage" runat="server" CssClass="LABEL" Width="80px">File Image</asp:Label>&nbsp;
										<asp:DropDownList id="cboFileImage" runat="server" Width="238px"></asp:DropDownList></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3">
										<HR>
                                      
                                        &nbsp;
                                       
                                        &nbsp;
                                        &nbsp;&nbsp;
                                    </TD>
								</TR>
								<TR>
									<TD align="right">
										<asp:Button id="cmdOFACSanExclParameters" runat="server" Text="Submit"></asp:Button></TD>
									<TD></TD>
									<TD><INPUT language="javascript" id="cmdClose" onclick="return cmdClose_onclick()" type="button"
											value="Close" name="cmdClose">
									</TD>
								</TR>
							</TABLE>
						</asp:panel>
					</td>
				</tr>
				<tr>
					<td></td>
					<td vAlign="top">
					<CR:CRYSTALREPORTVIEWER id="rptViewer" runat="server" Height="50px" Width="350px" BorderStyle="None" AutoDataBind="true"
							PrintMode="ActiveX" ToolPanelView="None" HasToggleParameterPanelButton="False"></CR:CRYSTALREPORTVIEWER>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
