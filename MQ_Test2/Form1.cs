﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MQExample
{
  public partial class Form1 : Form
  {
    MQTest myMQ = new MQTest();
    public Form1()
    {
      InitializeComponent();
      txtQueueManagerName.Text = "REZA_QM";
      txtPUTQueueName.Text = "KIKI.REMOTE";
      txtGETQueueName.Text = "REZA.LISTEN";
      txtChannelInfo.Text = "SVRCONN/TCP/localhost(1424)";
    }

    private void btnConnect_Click(object sender, EventArgs e)
    {
      string strQueueManagerName;
      string strQueueName;
      string strChannelInfo;

      txtGET.Text = "";
      txtPUT.Text = "";
      
      //TODO
      //PUT Valication Code Here
      
      strQueueManagerName = txtQueueManagerName.Text;
      strQueueName = txtPUTQueueName.Text;
      strChannelInfo = txtChannelInfo.Text;
      lblConnect.Text = myMQ.ConnectMQ(strQueueManagerName, strChannelInfo);
    }

    private void btnWriteMsg_Click(object sender, EventArgs e)
    {
      txtPUT.Text = myMQ.WriteLocalQMsg(txtPUT.Text.ToString(),
        txtPUTQueueName.Text.ToString());
    }

    private void button2_Click(object sender, EventArgs e)
    {
      txtGET.Text = myMQ.ReadLocalQMsg(txtGETQueueName.Text.ToString());
    }
  }
}
