﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Newtonsoft.Json;

namespace WC1_API
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string gatewayhost = String.Empty;
        private string gatewayurl = String.Empty;
        private string apikey = String.Empty;
        private string apiurl = String.Empty;
        private string apisecret = String.Empty;
        private string secondaryFields = String.Empty;
        private string entityType = String.Empty;
        private string customFields = String.Empty;
        private string typeid = String.Empty;
        private string groupId = String.Empty;
        private string providerTypes = String.Empty;
        private int basenum = 0;
        private string strProxy = String.Empty;
        private string str_1000 = String.Empty;
        private string TR1 = String.Empty;
        private string TR2 = String.Empty;
        private string TR3 = String.Empty;
        private string str_exclude = String.Empty;
        private int apiretry = 5;
        private int retrySec = 10;

        private void button1_Click(object sender, EventArgs e)
        {
            textBox12.Text = "";
            textBox13.Text = "";

            //System.Threading.Thread.Sleep(1000);

            string str_id = "0102TE" + DateTime.Now.ToString("YYYYMMDD HHmmss");
            string str_casesysid = "";
            try
            {
                str_casesysid = post_scansanctiondata(str_id, "Q", textBox14.Text.Trim(), str_id);

                textBox12.Text = post_scansanctiondata(str_casesysid, "S", textBox14.Text.Trim(), str_id);

                ArrayList wcresult = new ArrayList();

                wcresult = wc_getresult(str_casesysid, "");
                if (wcresult != null && wcresult.Count > 0)
                {
                    textBox13.Text = "Match Count:" + wcresult.Count.ToString();
                }
                else
                {
                    textBox13.Text = "No match";
                }
            }
            catch(Exception ex)
            {
                textBox12.Text = ex.Message;
            }
        }

        private string post_scansanctiondata(string caseSystemId, string str_Mode, string strsanctiondata, string refstr)
        {

            DateTime dateValue = DateTime.UtcNow; // get the datetime NOW GMT

            string date = dateValue.ToString("R"); // WC1 header requires GMT datetime stamp

            string requestendpoint = apiurl;
            if (str_Mode == "S")
            {
                requestendpoint = requestendpoint + "/" + caseSystemId + "/screeningRequest";
            }

            string strref = refstr;

            customFields = "[{\"typeId\":\"" + typeid + "\",\"value\":\"" + strref + "\"}]";

            //customFields = "[]";

            string postData = "{\"secondaryFields\":" + secondaryFields + ",\"entityType\":\"" + entityType + "\",\"customFields\":" + customFields + ",\"groupId\":\"" + groupId + "\",\"providerTypes\":" + providerTypes + ",\"name\":\"" + strsanctiondata + "\"}";

            string msg = postData;

            UTF8Encoding encoding = new UTF8Encoding();
            byte[] byte1 = encoding.GetBytes(postData);

            // Assemble the POST request - NOTE every character including spaces have to be EXACT 
            // for the API server to decode the authorization signature       
            string strUrl = requestendpoint.Replace("https://" + gatewayhost, "");

            string dataToSign = "(request-target): post " + strUrl + "\n" +
                "host: " + gatewayhost + "\n" +   // no https only the host name
                "date: " + date + "\n" +          // GMT date as a string
                "content-type: " + "application/json" + "\n" +
                "content-length: " + byte1.Length + "\n" +
                 msg;

            /*
            LogAdd("---api secret---");
            LogAdd(fortifyService.PathManipulation(apisecret));
            LogAdd("---dataToSign---");
            LogAdd(fortifyService.PathManipulation(dataToSign));
            LogAdd("string hmac = generateAuthHeader(dataToSign, apisecret);");
            */
            // The Request and API secret are now combined and encrypted
            string hmac = generateAuthHeader(dataToSign, apisecret);

            // Assemble the authorization string - This needs to match the dataToSign elements 
            // i.e. requires  host date content-type content-length
            //- NOTE every character including spaces have to be EXACT else decryption will fail with 401 Unauthorized
            string authorisation = "Signature keyId=\"" + apikey + "\",algorithm=\"hmac-sha256\",headers=\"(request-target) host date content-type content-length\",signature=\"" + hmac + "\"";
            /*
            LogAdd("---Hmac---");
            LogAdd(hmac);
            */
            // Send the Request to the API server
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(requestendpoint);

            // Set the Headers
            WebReq.Method = "POST";
            WebReq.Headers.Add("Authorization", authorisation);
            WebReq.Headers.Add("Cache-Control", "no-cache");
            WebReq.ContentLength = msg.Length;

            // Set the content type of the data being posted.
            Type type = WebReq.Headers.GetType();
            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            MethodInfo methodInfo = type.GetMethod("AddWithoutValidate", flags);
            object[] myPara = new object[2];
            myPara[0] = "Date";
            myPara[1] = dateValue.ToString("R");
            methodInfo.Invoke(WebReq.Headers, myPara);

            WebReq.ContentType = "application/json";
            WebReq.ContentLength = byte1.Length;

            //proxy
            strProxy = textBox15.Text;
            if (strProxy != String.Empty)
            {
                string[] strByPass = new string[] { @"192.\.168\..*", @"172.*" };
                WebReq.Proxy = new WebProxy(new Uri(strProxy), true, strByPass);
            }

            //SSL TLS1.2
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return CheckCert();
            };


            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;


            Stream newStream = WebReq.GetRequestStream();
            newStream.Write(byte1, 0, byte1.Length);

            bool bol_success = false;

            //2018.03 add api retry function
            for (int i = 0; i < apiretry; i++)
            {
                if (bol_success == true)
                {
                    break;
                }
                else
                {
                    System.Threading.Thread.Sleep(1000 * retrySec);
                    //LogAdd("Try times:" + i);
                }
                try
                {
                    // Get the Response - Status OK
                    HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                    // Status information about the request

                    // Get the Response data
                    Stream Answer = WebResp.GetResponseStream();
                    StreamReader _Answer = new StreamReader(Answer);
                    string jsontxt = _Answer.ReadToEnd();

                    textBox12.Text = jsontxt;

                    //LogAdd("return value:" + jsontxt);

                    // convert json text to a pretty printout
                    var obj = JsonConvert.DeserializeObject(jsontxt);
                    var f = JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented);

                    if (str_Mode == "Q")
                    {
                        wcapi wcapi = JsonConvert.DeserializeObject<wcapi>(f);
                        caseSystemId = wcapi.caseSystemId;
                        //LogAdd("caseSysID:" + caseSystemId);
                    }
                    if (str_Mode == "S")
                    {
                        caseSystemId = f;
                    }
                    bol_success = true;
                }
                catch (WebException ex)
                {
                    //Logerr(ex.ToString());
                    textBox12.Text = ex.Message;
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        //Logerr("WebException code:" + ((int)response.StatusCode).ToString());
                        textBox13.Text = response.StatusCode.ToString();
                    }
                }
            }
            return caseSystemId;
        }

        private ArrayList wc_getresult(string caseSystemId, string StrEntID)
        {
            DateTime dateValue = DateTime.UtcNow; // get the datetime NOW GMT

            string date = dateValue.ToString("R"); // WC1 header requires GMT datetime stamp

            //set host and credentials to the WC1 API Pilot server WC1SampleClientAPI account

            string requestendpoint = apiurl + "/" + caseSystemId + "/results";

            if (StrEntID != "")
            {
                requestendpoint = apiurl.Replace("cases", "reference/profile/") + StrEntID;
            }

            // Assemble the GET request - NOTE every character including spaces have to be EXACT 
            // for the API server to decode the authorization signature       
            string strUrl = requestendpoint.Replace("https://" + gatewayhost, "");
            if (strUrl.IndexOf('?') > 0)
                strUrl = strUrl.Replace(strUrl.Substring(strUrl.IndexOf('?')), "");
            string dataToSign = "(request-target): get " + strUrl + "\n" +
                "host: " + gatewayhost + "\n" +   // no https only the host name
                "date: " + date;                  // GMT date as a string
            // The Request and API secret are now combined and encrypted
            string hmac = generateAuthHeader(dataToSign, apisecret);

            // Assemble the authorization string - This needs to match the dataToSign elements 
            // i.e. requires ONLY host date (no content body for a GET request)
            //- NOTE every character including spaces have to be EXACT else decryption will fail with 401 Unauthorized
            string authorisation = "Signature keyId=\"" + apikey + "\",algorithm=\"hmac-sha256\",headers=\"(request-target) host date\",signature=\"" + hmac + "\"";

            // Send the Request to the API server
            HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(requestendpoint);
            // Set the Headers
            WebReq.Method = "GET";
            WebReq.Headers.Add("Authorization", authorisation);
            WebReq.Headers.Add("Cache-Control", "no-cache");
            Type type = WebReq.Headers.GetType();
            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            MethodInfo methodInfo = type.GetMethod("AddWithoutValidate", flags);
            object[] myPara = new object[2];
            myPara[0] = "Date";
            myPara[1] = dateValue.ToString("R");
            methodInfo.Invoke(WebReq.Headers, myPara);

            //proxy
            strProxy = textBox15.Text;
            if (strProxy != String.Empty)
            {
                string[] strByPass = new string[] { @"192.\.168\..*", @"172\.17\..*" };
                WebReq.Proxy = new WebProxy(new Uri(strProxy), true, strByPass);
            }

            //SSL TLS1.2
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return CheckCert();
            };

            //for SSL

            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;


            try
            {
                // Get the Response - Status OK
                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                // Status information about the request

                // Get the Response data
                Stream Answer = WebResp.GetResponseStream();
                StreamReader _Answer = new StreamReader(Answer);
                string jsontxt = _Answer.ReadToEnd();

                // convert json text to a pretty printout
                var obj = JsonConvert.DeserializeObject(jsontxt);
                var f = JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented);

                textBox12.Text = jsontxt;

                ArrayList result = new ArrayList();

                if (StrEntID == "")
                {
                    result = JsonConvert.DeserializeObject<ArrayList>(f);
                }
                else
                {
                    Profile prof = JsonConvert.DeserializeObject<Profile>(f);

                    for (int i = 0; i < prof.names.Count(); i++)
                    {
                        result.Add(prof.names[i]);
                    }
                }

                return result;
            }
            catch (WebException ex)
            {
                textBox12.Text = ex.Message;
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    textBox13.Text = response.StatusCode.ToString();
                }
                return null;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            gatewayhost = textBox1.Text.Trim();
            gatewayurl = textBox2.Text.Trim();
            apikey = textBox3.Text.Trim();
            apiurl = textBox4.Text.Trim();
            apisecret = textBox5.Text.Trim();
            secondaryFields = textBox6.Text.Trim();
            entityType = textBox7.Text.Trim();
            customFields = textBox8.Text.Trim();
            typeid = textBox9.Text.Trim();
            groupId = textBox10.Text.Trim();
            providerTypes = textBox11.Text.Trim();
            basenum = 20000000;
            strProxy = textBox15.Text;
            
            apiretry = 3;
            retrySec = 10;
        }

        private static string generateAuthHeader(string dataToSign, string apisecret)
        {
            byte[] secretKey = Encoding.UTF8.GetBytes(apisecret);
            HMACSHA256 hmac = new HMACSHA256(secretKey);
            hmac.Initialize();

            byte[] bytes = Encoding.UTF8.GetBytes(dataToSign);
            byte[] rawHmac = hmac.ComputeHash(bytes);
            hmac.Clear();
            return (Convert.ToBase64String(rawHmac));

        }

        public Boolean CheckCert()
        {
            Boolean bol_r = false;

            if ("a" == "a")
            {
                bol_r = true;
            }

            return bol_r;
        }

        public class wcapi
        {
            public string caseId { get; set; }
            public string name { get; set; }

            public string[] providerTypes { get; set; }
            public custfields[] customFields { get; set; }
            public string[] secondaryFields { get; set; }
            public string groupId { get; set; }
            public string entityType { get; set; }
            public string caseSystemId { get; set; }
            public string caseScreeningState { get; set; }
            public string lifecycleState { get; set; }
            public creator creator { get; set; }
            public modifier modifier { get; set; }
            public string creationDate { get; set; }
            public string modificationDate { get; set; }
            public string outstandingActions { get; set; }
        }

        public class custfields
        {
            public string typeId { get; set; }
            public string value { get; set; }
            public string dateTimeValue { get; set; }
        }

        public class creator
        {
            public string userId { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string email { get; set; }
            public string status { get; set; }
        }

        public class modifier
        {
            public string userId { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string email { get; set; }
            public string status { get; set; }

        }

        public class ResultDetail
        {
            public string resultId { get; set; }
            public string referenceId { get; set; }
            public string matchStrength { get; set; }
            public string matchedTerm { get; set; }
            public string submittedTerm { get; set; }
            public string matchedNameType { get; set; }
            public string[] secondaryFieldResults { get; set; }
            public string[] sources { get; set; }
            public string[] categories { get; set; }
            public string creationDate { get; set; }
            public string modificationDate { get; set; }
            public string resolution { get; set; }
            public ResultReview resultReview { get; set; }
        }

        public class ResultReview
        {
            public string reviewRequired { get; set; }
            public string reviewRequiredDate { get; set; }
            public string reviewRemark { get; set; }
            public string reviewDate { get; set; }
        }

        public class Profile
        {
            public string entityId { get; set; }
            public Names[] names { get; set; }
        }

        public class Names
        {
            public string fullName { get; set; }
            public string givenName { get; set; }
            public LanguageCode languageCode { get; set; }
            public string lastName { get; set; }
            public string originalScript { get; set; }
            public string prefix { get; set; }
            public string suffix { get; set; }
            public string type { get; set; }
        }

        public class LanguageCode
        {
            public string code { get; set; }
            public string name { get; set; }
        }

        private void textBox14_FontChanged(object sender, EventArgs e)
        {

        }

        private void textBox14_KeyDown(object sender, KeyEventArgs e)
        {
            textBox12.Text = "";
            textBox13.Text = "";
        }
    }
}
