﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using Ionic.Zip;
using Ionic.Zlib;
using System.ServiceModel;
using System.Net;
using System.Net.Security;
using System.Runtime.Serialization;
using Microsoft.VisualBasic.FileIO;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Algorithms;
using static ConsoleApp2.Program;
using System.Text.RegularExpressions;
using static System.Net.Mime.MediaTypeNames;
using Microsoft.VisualBasic;


namespace ConsoleApp2
{
    public class Program
    {
        static Database database;
        static void Main(string[] args)
        {
            database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

            Console.WriteLine(SHA256("010202"));

            //TestLevenDist();

            //TestReadLine();

            //testdbcommand();
            //checkReg();

            //readXML();

            //readfiles();

            //CheckMatch();

            //Test_command();
            //Testlist2();
            //testlist();

            //Console.WriteLine(GetGACode());
            //ZipfilesByweek();

            //string input = Console.ReadLine();

            //TestBase64(input);

            //MXCuttext();

            //testWLR();
            //Cuttext();
            //Cut_CIF();
            //testCoding();
            //ReadNativeFile();
            //xmltoDB();
            //readexcel();

            //GetSwiftTag();

            //string a = CheckCharacter("65998");

            //CheckMatchText();

            Console.WriteLine("End:" + DateTime.Now.Minute.ToString().PadLeft(2, '0'));
            Console.ReadLine();

        }

        private static long mvarSecurityToken;

        public Program(long SecurityToken) => mvarSecurityToken = SecurityToken;

        public static long SecurityToken => mvarSecurityToken;

        private static long MLBits(int iBit) => checked((long)Math.Round(unchecked(Math.Pow(2.0, (double)checked(iBit + 1)) - 1.0)));

        private static long TwoInPower(int iBit) => checked((long)Math.Round(Math.Pow(2.0, unchecked((double)iBit))));

        private static long LShift(long lValue, int iShiftBits)
        {
            long num1;
            long num2;
            switch (iShiftBits)
            {
                case 0:
                    num1 = (double)mvarSecurityToken <= 10000000.0 ? 0L : lValue;
                    goto label_6;
                case 31:
                    num2 = ((ulong)lValue & 1UL) <= 0UL ? 0L : (long)int.MinValue;
                    break;
                default:
                    if (iShiftBits < 0 | iShiftBits > 31)
                    {
                        num2 = 0L;
                        break;
                    }
                    break;
            }
            num1 = (lValue & TwoInPower(checked(31 - iShiftBits))) == 0L ? checked(lValue & MLBits(31 - iShiftBits) * TwoInPower(iShiftBits)) : checked(lValue & MLBits(31 - (iShiftBits + 1)) * TwoInPower(iShiftBits)) | (long)int.MinValue;
        label_6:
            return num1;
        }

        private static long RShift(long lValue, int iShiftBits)
        {
            long num1;
            long num2;
            switch (iShiftBits)
            {
                case 0:
                    num1 = lValue;
                    break;
                case 31:
                    num2 = ((ulong)lValue & 18446744071562067968UL) <= 0UL ? 0L : 1L;
                    goto label_7;
                default:
                    if (iShiftBits < 0 | iShiftBits > 31)
                    {
                        num1 = 0L;
                        break;
                    }
                    break;
            }
            num2 = (lValue & 2147483646L) / TwoInPower(iShiftBits);
            if (((ulong)lValue & 18446744071562067968UL) > 0UL)
                num2 |= 1073741824L / TwoInPower(checked(iShiftBits - 1));
            label_7:
            return num2;
        }

        private static long[] SplitCharacters(string sMessage)
        {
            long lValue1 = (long)Strings.Len(sMessage);
            long num1 = checked(unchecked(checked(lValue1 + 8L) / 64L) + 1L * 16L);
            long[] numArray = new long[checked((int)(num1 - 1L) + 1)];
            long num2 = 0;
            while (num2 < lValue1)
            {
                long index = num2 / 4L;
                long iShiftBits = checked(3L - unchecked(num2 % 4L) * 8L);
                long lValue2 = (long)checked((byte)Strings.Asc(Strings.Mid(sMessage, (int)(num2 + 1L), 1)));
                numArray[checked((int)index)] = numArray[checked((int)index)] | LShift(lValue2, checked((int)iShiftBits));
                checked { ++num2; }
            }
            long index1 = num2 / 4L;
            long iShiftBits1 = checked(3L - unchecked(num2 % 4L) * 8L);
            numArray[checked((int)index1)] = numArray[checked((int)index1)] | LShift(128L, checked((int)iShiftBits1));
            numArray[checked((int)(num1 - 1L))] = LShift(lValue1, 3);
            numArray[checked((int)(num1 - 2L))] = RShift(lValue1, 29);
            return numArray;
        }

        private static long AddULong(long lX, long lY)
        {
            long num1 = lX & (long)int.MinValue;
            long num2 = lY & (long)int.MinValue;
            long num3 = lX & 1073741824L;
            long num4 = lY & 1073741824L;
            long num5 = checked(lX & 1073741823L + lY & 1073741823L);
            return (num3 & num4) == 0L ? ((num3 | num4) == 0L ? num5 ^ num1 ^ num2 : (((ulong)num5 & 1073741824UL) <= 0UL ? num5 ^ 1073741824L ^ num1 ^ num2 : num5 ^ -1073741824L ^ num1 ^ num2)) : num5 ^ (long)int.MinValue ^ num1 ^ num2;
        }

        private static long Ch(long x, long y, long z) => x & y ^ ~x & z;

        private static long Maj(long x, long y, long z) => x & y ^ x & z ^ y & z;

        private static long s(long x, long n) => RShift(x, checked((int)(n & MLBits(4)))) | LShift(x, checked((int)(32L - (n & MLBits(4)))));

        private static long R(long x, long n) => RShift(x, checked((int)(n & MLBits(4))));

        private static long Sigma0(long x) => s(x, 2L) ^ s(x, 13L) ^ s(x, 22L);

        private static long Sigma1(long x) => s(x, 6L) ^ s(x, 11L) ^ s(x, 25L);

        private static long Gamma0(long x) => s(x, 7L) ^ s(x, 18L) ^ R(x, 3L);

        private static long Gamma1(long x) => s(x, 17L) ^ s(x, 19L) ^ R(x, 10L);

        public static string SHA256(string sMessage)
        {
            mvarSecurityToken = 72832867;
            long[] numArray1 = new long[8];
            long[] numArray2 = new long[64];
            long[] numArray3 = new long[64];
            numArray1[0] = 1779033703L;
            numArray1[1] = -1150833019L;
            numArray1[2] = 1013904242L;
            numArray1[3] = -1521486534L;
            numArray1[4] = 1359893119L;
            numArray1[5] = -1694144372L;
            numArray1[6] = 528734635L;
            numArray1[7] = 1541459225L;
            numArray2[0] = 1116352408L;
            numArray2[1] = 1899447441L;
            numArray2[2] = -1245643825L;
            numArray2[3] = -373957723L;
            numArray2[4] = 961987163L;
            numArray2[5] = 1508970993L;
            numArray2[6] = -1841331548L;
            numArray2[7] = -1424204075L;
            numArray2[8] = -670586216L;
            numArray2[9] = 310598401L;
            numArray2[10] = 607225278L;
            numArray2[11] = 1426881987L;
            numArray2[12] = 1925078388L;
            numArray2[13] = -2132889090L;
            numArray2[14] = -1680079193L;
            numArray2[15] = -1046744716L;
            numArray2[16] = -459576895L;
            numArray2[17] = -272742522L;
            numArray2[18] = 264347078L;
            numArray2[19] = 604807628L;
            numArray2[20] = 770255983L;
            numArray2[21] = 1249150122L;
            numArray2[22] = 1555081692L;
            numArray2[23] = 1996064986L;
            numArray2[24] = -1740746414L;
            numArray2[25] = -1473132947L;
            numArray2[26] = -1341970488L;
            numArray2[27] = -1084653625L;
            numArray2[28] = -958395405L;
            numArray2[29] = -710438585L;
            numArray2[30] = 113926993L;
            numArray2[31] = 338241895L;
            numArray2[32] = 666307205L;
            numArray2[33] = 773529912L;
            numArray2[34] = 1294757372L;
            numArray2[35] = 1396182291L;
            numArray2[36] = 1695183700L;
            numArray2[37] = 1986661051L;
            numArray2[38] = -2117940946L;
            numArray2[39] = -1838011259L;
            numArray2[40] = -1564481375L;
            numArray2[41] = -1474664885L;
            numArray2[42] = -1035236496L;
            numArray2[43] = -949202525L;
            numArray2[44] = -778901479L;
            numArray2[45] = -694614492L;
            numArray2[46] = -200395387L;
            numArray2[47] = 275423344L;
            numArray2[48] = 430227734L;
            numArray2[49] = 506948616L;
            numArray2[50] = 659060556L;
            numArray2[51] = 883997877L;
            numArray2[52] = 958139571L;
            numArray2[53] = 1322822218L;
            numArray2[54] = 1537002063L;
            numArray2[55] = 1747873779L;
            numArray2[56] = 1955562222L;
            numArray2[57] = 2024104815L;
            numArray2[58] = -2067236844L;
            numArray2[59] = -1933114872L;
            numArray2[60] = -1866530822L;
            numArray2[61] = -1538233109L;
            numArray2[62] = -1090935817L;
            numArray2[63] = -965641998L;
            long[] numArray4 = SplitCharacters(sMessage);
            long num1 = (long)Information.UBound((Array)numArray4);
            long num2 = 0;
            while (num2 <= num1)
            {
                long num3 = numArray1[0];
                long num4 = numArray1[1];
                long num5 = numArray1[2];
                long lX1 = numArray1[3];
                long num6 = numArray1[4];
                long num7 = numArray1[5];
                long num8 = numArray1[6];
                long lX2 = numArray1[7];
                long index = 0;
                do
                {
                    numArray3[checked((int)index)] = index >= 16L ? AddULong(AddULong(AddULong(Gamma1(numArray3[checked((int)(index - 2L))]), numArray3[checked((int)(index - 7L))]), Gamma0(numArray3[checked((int)(index - 15L))])), numArray3[checked((int)(index - 16L))]) : numArray4[checked((int)(index + num2))];
                    long num9 = AddULong(AddULong(AddULong(AddULong(lX2, Sigma1(num6)), Ch(num6, num7, num8)), numArray2[checked((int)index)]), numArray3[checked((int)index)]);
                    long lY = AddULong(Sigma0(num3), Maj(num3, num4, num5));
                    lX2 = num8;
                    num8 = num7;
                    num7 = num6;
                    num6 = AddULong(lX1, num9);
                    lX1 = num5;
                    num5 = num4;
                    num4 = num3;
                    num3 = AddULong(num9, lY);
                    checked { ++index; }
                }
                while (index <= 63L);
                numArray1[0] = AddULong(num3, numArray1[0]);
                numArray1[1] = AddULong(num4, numArray1[1]);
                numArray1[2] = AddULong(num5, numArray1[2]);
                numArray1[3] = AddULong(lX1, numArray1[3]);
                numArray1[4] = AddULong(num6, numArray1[4]);
                numArray1[5] = AddULong(num7, numArray1[5]);
                numArray1[6] = AddULong(num8, numArray1[6]);
                numArray1[7] = AddULong(lX2, numArray1[7]);
                checked { num2 += 16L; }
            }
            return Strings.LCase(Strings.Right("00000000" + Conversion.Hex(numArray1[0]), 8) + Strings.Right("00000000" + Conversion.Hex(numArray1[1]), 8) + Strings.Right("00000000" + Conversion.Hex(numArray1[2]), 8) + Strings.Right("00000000" + Conversion.Hex(numArray1[3]), 8) + Strings.Right("00000000" + Conversion.Hex(numArray1[4]), 8) + Strings.Right("00000000" + Conversion.Hex(numArray1[5]), 8) + Strings.Right("00000000" + Conversion.Hex(numArray1[6]), 8) + Strings.Right("00000000" + Conversion.Hex(numArray1[7]), 8));
        }

        public static void TestLevenDist()
        {
            float iWordDistance = 0;
            float iNorWordDistance = 0;
            LevenshteinDistance LS = new LevenshteinDistance();

            string str_MatchName = "RUNGTAWANKHIRI,NAT";
            string str_MatchText = "RUNGTAWANKHERE, Nat";

            iWordDistance = LS.Calculate(str_MatchName, str_MatchText);
            Console.WriteLine("MatchName=" + str_MatchName);
            Console.WriteLine("MatchText=" + str_MatchText);
            Console.WriteLine("WordDistance=" + iWordDistance.ToString());

            int iMaxLen = str_MatchText.Length;
            if (str_MatchText.Length < str_MatchName.Length)
            {
                iMaxLen = str_MatchName.Length;
            }
            float iOriScore = (1 - (iWordDistance / iMaxLen)) * 100;
            


            string strNor_MatchName = Regex.Replace(str_MatchName, "[^a-zA-Z0-9]+", "", RegexOptions.Compiled);
            string strNor_MatchText = Regex.Replace(str_MatchText, "[^a-zA-Z0-9]+", "", RegexOptions.Compiled); ;

            iNorWordDistance = LS.Calculate(strNor_MatchName, strNor_MatchText);
            Console.WriteLine("Normalized MatchName=" + strNor_MatchName);
            Console.WriteLine("Normalized MatchText=" + strNor_MatchText);
            Console.WriteLine("Normalized WordDistance=" + iNorWordDistance.ToString());
            int iNorMaxLen = strNor_MatchText.Length;
            if (strNor_MatchText.Length < strNor_MatchName.Length)
            {
                iNorMaxLen = strNor_MatchName.Length;
            }
            float iNorScore = (1 - (iNorWordDistance / iNorMaxLen)) * 100;
            Console.WriteLine("Original Score=" + Math.Round(iOriScore,2).ToString());
            Console.WriteLine("Normalized Score=" + Math.Round(iNorScore, 2).ToString());

            Console.WriteLine("Final Score=" + Math.Round(((iOriScore + iNorScore) / 2),0).ToString());
        }

        public static void TestReadLine()
        {
            try
            {
                int ia = 0;
                int ib = 5;

                if (ib/ ia > 0)
                {
                    ia = ib;
                }

                string filePathName = ConfigurationManager.AppSettings["TestReadXML"];

                StreamReader reader = new StreamReader(filePathName);

                string stra = "";
                string strb = "";
                
                while ((stra = ReadLineSafe(reader, int.MaxValue)) != null)
                {
                    strb += stra;
                }
                StreamWriter sw = new StreamWriter(@"D:\WorldCheck\xmlmanual_append_first.txt");
                sw.WriteLine(strb);
                sw.Close();

                /*
                while ((stra = (reader.ReadLine())) != null)
                {
                    strb += stra;
                }
                

                StreamWriter sw = new StreamWriter(@"D:\WorldCheck\xmldefault.txt");
                sw.WriteLine(strb);
                sw.Close();
                */
            }
            catch (Exception e)
            {
                if (e is DivideByZeroException)
                {
                    LogException(e, "TestReadLine");
                }

            }

        }

        public static string ReadLineSafe(TextReader reader, int maxLength)
        {
            var sb = new StringBuilder();
            bool bol_end = false;
            while (true)
            {
                int ch = reader.Read();
                if (ch == -1) break;
                if (ch == '\r' || ch == '\n')
                {
                    if (ch == '\r' && reader.Peek() == '\n')
                    {
                        bol_end = true;
                    }
                    else if(ch == '\r')
                    {
                        return sb.ToString();
                    }
                    if (ch == '\n')
                    {
                        return sb.ToString();
                    }
                }

                if (!bol_end)
                {
                    sb.Append((char)ch);
                }

                // Safety net here
                if (sb.Length > maxLength)
                {
                    return null;
                }
            }
            if (sb.Length > 0) return sb.ToString();
            return null;
        }

        private static bool LogException(Exception e, string str_FunctionName)
        {
            Console.WriteLine("Error at Function  " + str_FunctionName);
            Console.WriteLine("In the log routine. Caught " + e.GetType());
            Console.WriteLine("Message: " + e.Message);
            return true;
        }

        static void testdbcommand()
        {
            database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

            string SQL = "exec OFAC..OFS_InsEvent @oper, @type, @objectType, @objectId, @logText";
            string s_result = "";
            using (DbCommand dBCommand = database.GetSqlStringCommand(SQL))
            {
                database.AddInParameter(dBCommand, "@oper", DbType.String, "primeadmin");
                database.AddInParameter(dBCommand, "@type", DbType.String, "Ann");
                database.AddInParameter(dBCommand, "@objectType", DbType.String, "Event");
                database.AddInParameter(dBCommand, "@objectId", DbType.String, "Test");
                database.AddInParameter(dBCommand, "@logText", DbType.String, "TestEventCommand");

                try
                {
                    IDataReader datareader = database.ExecuteReader(dBCommand);

                    while (datareader.Read())
                    {
                        s_result = datareader[0].ToString();
                    }
                }

                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    dBCommand.Dispose();
                }

            }
        }

        static void checkReg()
        {
            string A = "{INT-BIC6}FBMECY";

            string B = A.Substring(A.IndexOf("}") + 1);
            string SwallowWSTimeout = "A";

            int ws_timeout = 300;

            if (!string.IsNullOrEmpty(SwallowWSTimeout))
            {
                int.TryParse(SwallowWSTimeout, out ws_timeout);
            }

            var test = DateTime.Now.ToString("MMM");

            Console.WriteLine(ws_timeout);

        }

        static void readXML()
        {
            string filePathName = ConfigurationManager.AppSettings["TestReadXML"];

            StreamReader sr = new StreamReader(filePathName);
            string str_lines_header = "";
            string str_lines_doc = "";
            string line = "";
            bool bol_check_part1 = false;
            bool bol_check_part2 = false;

            while (!sr.EndOfStream)
            {               
                // 每次讀取一行，直到檔尾
                line = sr.ReadLine();
                if (line.Contains("<AppHdr"))
                {
                    bol_check_part1 = true;
                }
                else if (line.Contains("<Document"))
                {
                    bol_check_part1 = false;
                    bol_check_part2 = true;
                }
                
                if (bol_check_part1)
                {
                    str_lines_header += line;
                }

                if (bol_check_part2)
                {
                    str_lines_doc += line;
                }

                Console.WriteLine(line);
            }
            sr.Close();

            //header
            TextReader textreaderHeader = new StringReader(str_lines_header);
            //XmlReader XmlreaderHeader = XmlReader.Create(new System.IO.StringReader(str_lines_header));

            using (XmlTextReader reader = new XmlTextReader(textreaderHeader))
            {
                reader.Namespaces = false;

                XmlSerializer H_serializer = new XmlSerializer(typeof(TransHeader));
                TransHeader Headerresult = (TransHeader)H_serializer.Deserialize(reader);
            }


            //doc
            TextReader textreaderDoc = new StringReader(str_lines_doc);
            //XmlReader XmlreaderDoc = XmlReader.Create(new System.IO.StringReader(str_lines_doc));
 
            using (XmlTextReader reader = new XmlTextReader(textreaderDoc))
            {
                reader.Namespaces = false;

                XmlSerializer C_serializer = new XmlSerializer(typeof(Contain));
                Contain Containresult = (Contain)C_serializer.Deserialize(reader);
            }
            
        }

        [XmlRootAttribute("AppHdr")]
        public class TransHeader
        {
            [XmlElement("Fr")]
            public Fr fr { get; set; }

            [XmlElement("To")]
            public To to { get; set; }

            public string BizMsgIdr { get; set; }
            public string MsgDefIdr { get; set; }
            public string BizSvc { get; set; }
            public string CreDt { get; set; }
        }

        public class Fr
        {
            [XmlElement("FIId")]
            public FIId fiid { get; set; }
        }

        public class To
        {
            [XmlElement("FIId")]
            public FIId fiid { get; set; }
        }

        public class FIId
        {
            [XmlElement("FinInstnId")]
            public FinInstnId fininstnid { get; set; }
        }

        public class FinInstnId
        {
            public string BICFI { get; set; }
        }

        [XmlRootAttribute("Document")]
        public class Contain
        {
            [XmlElement("RsltnOfInvstgtn")]
            public RsltnOfInvstgtn rstnofinvstgtn { get; set; }
        }

        public class RsltnOfInvstgtn
        {
            [XmlElement("Assgnmt")]
            public Assgnmt assgnmt { get; set; }

            [XmlElement("Sts")]
            public Sts sts { get; set; }
            
        }

        public class Assgnmt
        {
            public string Id { get; set; }

            [XmlElement("Assgnr")]
            public Assgnr assgnr { get; set; }

            [XmlElement("Assgne")]
            public Assgne assgne { get; set; }

            public string CreDtTm { get; set; }
        }

        public class Assgnr
        {
            [XmlElement("Agt")]
            public Agt agt { get; set; }
        }

        public class Assgne
        {
            [XmlElement("Agt")]
            public Agt agt { get; set; }
        }

        public class Agt
        {
            [XmlElement("FinInstnId")]
            public FinInstnId fininstnid { get; set; }
        }

        public class Sts
        {
            public string Conf { get; set; }
        }

        static void readfiles()
        {
            string filePathName = ConfigurationManager.AppSettings["TestReadFile"];

            StreamReader sr = new StreamReader(filePathName);

            string line = "";
            string lines = "";
            string filename = "";
            int isubFolder = 1;
            int icount = 1;
            while (!sr.EndOfStream)
            {               // 每次讀取一行，直到檔尾
                //line = sr.ReadLine().Replace("-}$","");            // 讀取文字到 line 變數
                line = sr.ReadLine();
                Console.WriteLine(line);
            }
            sr.Close();

        }

        private static void CheckMatch()
        {
            string sqlcheck_matchtext = "";
            string strMatchtext = "/BENEFRES/AE123/XXXXXXXXXXXXXXXXX |MANSOUR, Muna Salim Saleh |H & H METALFORM GMBH";
            string strEngName = "H + H";

            sqlcheck_matchtext = @"select OFAC.dbo.[OFS_fnGetMatchText](@sSrhName, @sMatchName) ret";

            using (DbCommand dBCommand = database.GetSqlStringCommand(sqlcheck_matchtext))
            {
                database.AddInParameter(dBCommand, "@sSrhName", DbType.String, strMatchtext.Trim());
                database.AddInParameter(dBCommand, "@sMatchName", DbType.String, strEngName.Trim());

                try
                {
                    //May. 09, 2022 add exception catch
                    DataSet ds = database.ExecuteDataSet(dBCommand);

                    if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                    {
                        strMatchtext = ds.Tables[0].Rows[0]["ret"].ToString();
                    }

                   
                }

                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally
                {
                    dBCommand.Dispose();
                }
            }

        }

        private static DataTable GetSwiftTag()
        {


            DataTable dtr = new DataTable();
            string SQL = "";
            SQL = "select mt,tag,FieldName, Condition, [Format] from ofac..SWIFTTAG where isnull(Condition,'') != '' order by tag, mt";

            using (DbCommand cmd = database.GetSqlStringCommand(SQL))
            {
                cmd.CommandText = SQL;

                DataSet ds = database.ExecuteDataSet(cmd);

                dtr = ds.Tables[0];
            }

            DataRow[] dr = dtr.Select("MT =  'MT101'");

            return dtr;

        }

        public static void readexcel()
        {
            string fileName = ConfigurationManager.AppSettings["Excel_path"];

            ExcelReader excelreader = new ExcelReader();

            DataSet ds = new DataSet();
            SqlDataAdapter da;

            string Abbreviation = "";

            ds = excelreader.ReadExcel(fileName);

        }

        public static void xmltoDB()
        {
            string filePathName = ConfigurationManager.AppSettings["Native_FilePath"];

            //新建XML文件類別
            XmlDocument Xmldoc = new XmlDocument();
            //從指定的字串載入XML文件
            Xmldoc.Load(filePathName);
            //建立此物件，並輸入透過StringReader讀取Xmldoc中的Xmldoc字串輸出
            XmlReader Xmlreader = XmlReader.Create(new System.IO.StringReader(Xmldoc.OuterXml));
            //建立DataSet
            DataSet ds = new DataSet();
            //透過DataSet的ReadXml方法來讀取Xmlreader資料
            ds.ReadXml(Xmlreader);
            //建立DataTable並將DataSet中的第0個Table資料給DataTable
            DataTable dt_record = ds.Tables[0];
            Console.WriteLine("Records:" + dt_record.Rows.Count);
            DataTable dt_aliases = ds.Tables[1];
            Console.WriteLine("Aliases:" + dt_aliases.Rows.Count);
            DataTable dt_alias = ds.Tables[2];
            Console.WriteLine("Alias:" + dt_alias.Rows.Count);
            //回傳DataTable
            ds.Clear();
        }

        public static void ReadNativeFile()
        {
            XmlDocument doc = new XmlDocument();
            doc.XmlResolver = null;


            string filePathName = ConfigurationManager.AppSettings["Native_FilePath"];

            doc.Load(filePathName);

            XmlSerializer serializer = new XmlSerializer(typeof(List<Record>), new XmlRootAttribute("records"));

            string string_xml = doc.OuterXml.ToString();

            StreamWriter sw = new StreamWriter(@"D:\WorldCheck\xml.txt");
            sw.WriteLine(string_xml);
            sw.Close();

            StringReader stringReader = new StringReader(string_xml);

            List<Record> recordsList = (List<Record>)serializer.Deserialize(stringReader);

            Console.WriteLine(recordsList[0].foreign_aliases[0]);

        }

        public T Deserialize<T>(string xmlString) where T : class
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(T));
            using (TextReader reader = new StringReader(xmlString))
            {
                object deserializationObj = deserializer.Deserialize(reader);
                return deserializationObj as T;
            };
        }

        public class Record
        {

            public Foreign_aliases[] foreign_aliases { get; set; }
        }

        public class Foreign_aliases
        {
            public string[] foreign_alias { get; set; }
        }

        static void MXCuttext()
        {
            int Int_fileCount = 5000;
            Int_fileCount = int.Parse(ConfigurationManager.AppSettings["HKTEST_FileCount"]);
            string filePathName = ConfigurationManager.AppSettings["MX_FilePath"];

            StreamReader sr = new StreamReader(filePathName);

            string line = "";
            string lines = "";
            string filename = "";
            int isubFolder = 1;
            int icount = 1;
            while (!sr.EndOfStream)
            {               // 每次讀取一行，直到檔尾
                //line = sr.ReadLine().Replace("-}$","");            // 讀取文字到 line 變數
                line = sr.ReadLine();

                //filename = "MX_File" + icount.ToString().PadLeft(6, '0');
                lines += line + "\r\n";

                if(line.Contains("<UETR>"))
                {
                    filename = line.Substring(line.IndexOf("<UETR>") + 6, 16);
                }

                if (line.Contains("</Document>"))         // 103
                {
                    if (!File.Exists(ConfigurationManager.AppSettings["MX_FolderPath"] + isubFolder.ToString()))
                    {
                        Directory.CreateDirectory(ConfigurationManager.AppSettings["MX_FolderPath"] + isubFolder.ToString());
                    }

                    StreamWriter sw = new StreamWriter(ConfigurationManager.AppSettings["MX_FolderPath"] + isubFolder.ToString() + "/" + filename + ".xml");

                    sw.WriteLine(lines);
                    sw.Close();

                    icount += 1;
                    if (icount % Int_fileCount == 0)
                    {
                        isubFolder += 1;
                    }
                    lines = "";
                }
            }
            sr.Close();
        }

        /// <summary>
        /// cut swift
        /// </summary>
        static void Cuttext()
        {
            int Int_fileCount = 5000;
            Int_fileCount = int.Parse(ConfigurationManager.AppSettings["HKTEST_FileCount"]);
            string filePathName = ConfigurationManager.AppSettings["HKTEST_FilePath"];

            StreamReader sr = new StreamReader(filePathName);

            string line = "";
            string lines = "";
            string filename = "";
            int isubFolder = 1;
            int icount = 1;
            while (!sr.EndOfStream)
            {               // 每次讀取一行，直到檔尾
                //line = sr.ReadLine().Replace("-}$","");            // 讀取文字到 line 變數
                line = sr.ReadLine();
                if (line.Contains(":20:"))
                {
                    //filename = line.Replace(":20:", "");
                    filename = line.Substring(line.IndexOf(":20:") + 4);
                }

                lines += line + "\r\n";

                //if (line.Contains(":70:INV/123"))  // 202
                //if (line.Contains("-}"))         // 103
                if (line.Contains("$"))            //Oct.23.2024
                {
                    //lines = lines.Substring(0, lines.LastIndexOf("-}"));
                    lines = lines.Substring(0, lines.LastIndexOf("$"));    //Oct.23.2024
                    if (!File.Exists(ConfigurationManager.AppSettings["HKTEST_CutFilePath"] + isubFolder.ToString()))
                    {
                        Directory.CreateDirectory(ConfigurationManager.AppSettings["HKTEST_CutFilePath"] + isubFolder.ToString());
                    }

                    StreamWriter sw = new StreamWriter(ConfigurationManager.AppSettings["HKTEST_CutFilePath"] + isubFolder.ToString() + "/" + filename + ".txt");

                    lines += "$";                 // 202
                    sw.WriteLine(lines);
                    sw.Close();
                    lines = line.Substring(line.LastIndexOf("$") + 1) + "\r\n";

                    icount += 1;
                    if (icount % Int_fileCount == 0)
                    {
                        isubFolder += 1;
                    }
                }
            }
            sr.Close();



        }

        static void Cut_CIF()
        {
            int Int_fileCount = 5000;
            string filePathName = ConfigurationManager.AppSettings["HKTEST_CIFFilePath"];
            Int_fileCount = int.Parse(ConfigurationManager.AppSettings["HKTEST_FileCount"]);
            StreamReader sr = new StreamReader(filePathName);

            string line = "";
            string lines = "";
            string filename = "";
            int isubFolder = 1;
            int icount = 1;
            while (!sr.EndOfStream)
            {               // 每次讀取一行，直到檔尾
                //line = sr.ReadLine().Replace("-}$","");            // 讀取文字到 line 變數
                line = sr.ReadLine();

                filename = line.Split('|')[0];



                //if (line.Contains(":70:INV/123"))  // 202

                if (!File.Exists(ConfigurationManager.AppSettings["HKTEST_CutFilePath"] + isubFolder.ToString()))
                {
                    Directory.CreateDirectory(ConfigurationManager.AppSettings["HKTEST_CutFilePath"] + isubFolder.ToString());
                }

                StreamWriter sw = new StreamWriter(ConfigurationManager.AppSettings["HKTEST_CutFilePath"] + isubFolder.ToString() + "/" + filename + ".txt", false, Encoding.GetEncoding(1252));

                sw.WriteLine(line.Trim());
                sw.Close();

                icount += 1;
                if (icount % Int_fileCount == 0)
                {
                    isubFolder += 1;
                }
            }
            sr.Close();
        }

        static void testCoding()
        {
            string fileName = "D:/WorldCheck/Country Code Translations.csv";
            int createdCount = 0;
            string[] List_header = null;
            var dataTable = new DataTable("TempWorldCheckDay");

            using (var textFieldParser = new TextFieldParser(fileName, System.Text.Encoding.GetEncoding(28605)))
            {
                textFieldParser.TextFieldType = FieldType.Delimited;
                textFieldParser.Delimiters = new[] { "," };
                while (!textFieldParser.EndOfData)
                {
                    createdCount++;

                    if (createdCount == 1)
                    {
                        List_header = textFieldParser.ReadFields();
                        foreach (string strHeader in List_header)
                        {
                            dataTable.Columns.Add(strHeader);
                        }
                        Console.WriteLine("total columns counts:" + dataTable.Columns.Count);


                    }

                    try
                    {
                        //break;

                        dataTable.Rows.Add(textFieldParser.ReadFields());
                        //dr = dataTable.Rows[createdCount - 1];
                    }
                    catch (Exception)
                    {
                    }
                }

                Console.WriteLine("total row counts:" + dataTable.Rows.Count);

                string sqlcheck_wccountry = @"select code, WorldCheck_Country from ofac..WorldCheckCountry";
                using (DbCommand dBCommand = database.GetSqlStringCommand(sqlcheck_wccountry))
                {
                    DataSet ds = database.ExecuteDataSet(dBCommand);
                    DataTable dt_wcnow = new DataTable();
                    dt_wcnow = ds.Tables[0];
                    try
                    {
                        if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                        {
                            foreach (DataRow dr_wcimport in dataTable.Rows)
                            {
                                if (dt_wcnow.Select(" code = '" + dr_wcimport["ISO3166-1"] + "' ").Length == 0)
                                {
                                    //wc_import country code not in wc_now country code
                                    //insert

                                }
                                else
                                {
                                    //check country name
                                    if (dt_wcnow.Select(" WorldCheck_Country = '" + dr_wcimport["CountryName"] + "' ").Length == 0)
                                    {
                                        //same country code, but country name no match
                                        //update country name

                                    }
                                }
                            }

                        }
                    }
                    finally
                    {
                        dBCommand.Dispose();
                    }
                }
            }

        }

        #region TestBase64

        private static void TestBase64(string src)
        {
            Encoding _encoding;

            Byte[] _base64Encode;

            string _base64String;
            _encoding = Encoding.GetEncoding("utf-8");
            //將檔案路徑進行 base64 編碼
            _base64Encode = _encoding.GetBytes(src);
            _base64String = Convert.ToBase64String(_base64Encode);
        }

        #endregion

        #region zipfiles

        private static void ZipfilesByweek()
        {

            DateTime dt_s = Convert.ToDateTime(ConfigurationManager.AppSettings["dateStart"].ToString());
            DateTime dt_e = Convert.ToDateTime(ConfigurationManager.AppSettings["dateEnd"].ToString());

            string str_folder_begin = @"D:\web_backup\CompressedFile";
            string str_foldername = "20180630";
            str_foldername = ConfigurationManager.AppSettings["foldername"];

            string strAMLinterface_log = @"AML Interface\log";
            string strAMLinterface_CIF = @"AML Interface\Backup\CIF";
            string strAMLinterface_Result = @"AML Interface\Backup\Result";
            string strAMLinterface_Swift = @"AML Interface\Backup\Swift";

            string strAMLImport_log = @"AMLImport\Log";
            string strAMLImport_CIF = @"AMLImport\Backup\CIF";
            string strAMLImport_SancParty = @"AMLImport\Backup\SancParty";

            string strBSAinterface_cust = @"BSA Interface\Customer";
            string strBSAinterface_acc = @"BSA Interface\Account";
            string strBSAinterface_act = @"BSA Interface\Activity";

            string strHostGW = @"HostGW\WinSwfAml\Backup";

            string strPrime_back = @"Prime\BOTOSB\Backup\PBSA";
            string strBranch = ConfigurationManager.AppSettings["Branch_no"];
            string strPrime_confirm = @"Prime\BOTOSB\" + strBranch + @"_SWIFT\Confirm";
            string strPrime_OK = @"Prime\BOTOSB\" + strBranch + @"_SWIFT\OK";

            string str_filename = "";
            try
            {
                while (int.Parse(dt_s.ToString("yyyyMMdd")) < int.Parse(dt_e.ToString("yyyyMMdd")))
                {
                    //amlinterface log
                    foreach (string file1 in Directory.GetFileSystemEntries(str_folder_begin + @"\" + str_foldername + @"\" + strAMLinterface_log))
                    {
                        str_filename = Path.GetFileName(file1);
                        if (!Directory.Exists(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd")))
                        {
                            Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd"));
                            if (!Directory.Exists(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + strAMLinterface_log))
                            {
                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strAMLinterface_log);
                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strAMLinterface_CIF);
                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strAMLinterface_Result);
                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strAMLinterface_Swift);

                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strAMLImport_log);
                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strAMLImport_CIF);
                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strAMLImport_SancParty);

                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strBSAinterface_cust);
                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strBSAinterface_acc);
                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strBSAinterface_act);

                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strHostGW);

                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strPrime_back);
                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strPrime_confirm);
                                Directory.CreateDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strPrime_OK);
                            }
                        }
                    }

                    Movefile(dt_s, str_folder_begin, str_foldername, strAMLinterface_log, "inlog");

                    //amlinterface cif
                    Movefile(dt_s, str_folder_begin, str_foldername, strAMLinterface_CIF, "incif");
                    //amlinterface result
                    Movefile(dt_s, str_folder_begin, str_foldername, strAMLinterface_Result, "inres");
                    //amlinterface swift
                    Movefile(dt_s, str_folder_begin, str_foldername, strAMLinterface_Swift, "insw");

                    //amlimport log
                    Movefile(dt_s, str_folder_begin, str_foldername, strAMLImport_log, "implog");
                    //amlimport cif
                    Movefile(dt_s, str_folder_begin, str_foldername, strAMLImport_CIF, "impcif");
                    //amlimport sancparty
                    Movefile(dt_s, str_folder_begin, str_foldername, strAMLImport_SancParty, "impsan");

                    //bsa customer
                    Movefile(dt_s, str_folder_begin, str_foldername, strBSAinterface_cust, "cust");
                    //bsa account
                    Movefile(dt_s, str_folder_begin, str_foldername, strBSAinterface_acc, "acc");
                    //bsa activity
                    Movefile(dt_s, str_folder_begin, str_foldername, strBSAinterface_act, "act");

                    //hostgw
                    Movefile(dt_s, str_folder_begin, str_foldername, strHostGW, "gw");

                    //prime bsa back
                    Movefile(dt_s, str_folder_begin, str_foldername, strPrime_back, "bsaback");
                    //prime swift confirm
                    Movefile(dt_s, str_folder_begin, str_foldername, strPrime_confirm, "conf");
                    //prime swift ok
                    Movefile(dt_s, str_folder_begin, str_foldername, strPrime_OK, "ok");


                    string zipFile = str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + ".zip";

                    ZipFile zip = new ZipFile(zipFile);
                    zip.AddDirectory(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd"));
                    zip.Save();

                    zip.Dispose();

                    File.SetLastWriteTime(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + ".zip", dt_s);

                    if (Directory.Exists(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd")))
                    {

                        Directory.Delete(str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd"), true);
                    }

                    dt_s = dt_s.AddDays(7);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ex:" + ex.ToString());
            }
        }

        private static void Movefile(DateTime dt_s, string str_folder_begin, string str_foldername, string strfilefolder, string strtype)
        {
            string str_filename = "";

            try
            {

                if (int.Parse(dt_s.ToString("yyyyMM")) == 201710)
                {
                    Console.WriteLine("got 201710");
                }


                foreach (string file1 in Directory.GetFileSystemEntries(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder))
                {
                    str_filename = Path.GetFileName(file1);

                    if (strtype == "inlog" && (int.Parse(str_filename.Substring(17, 10).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "incif" && (int.Parse(str_filename.Substring(7, 8).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "inres" && (int.Parse(str_filename.Substring(6, 8).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "insw" && (int.Parse(str_filename.Substring(6, 8).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "implog" && (int.Parse(str_filename.Substring(14, 10).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "impcif" && (int.Parse(str_filename.Substring(7, 8).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "impsan" && (int.Parse(str_filename.Substring(13, 10).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "cust" && (int.Parse(str_filename.Substring(13, 8).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "acc" && (int.Parse(str_filename.Substring(12, 8).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "act" && (int.Parse(str_filename.Substring(13, 8).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "gw" && (int.Parse(str_filename.Substring(6, 8).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "bsaback" && str_filename.ToUpper().Contains("CUSTOMER") && (int.Parse(str_filename.Substring(13, 8).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "bsaback" && str_filename.ToUpper().Contains("ACCOUNT") && (int.Parse(str_filename.Substring(12, 8).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "bsaback" && str_filename.ToUpper().Contains("ACTIVITY") && (int.Parse(str_filename.Substring(13, 8).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "conf" && (int.Parse(str_filename.Substring(6, 8).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }

                    if (strtype == "ok" && (int.Parse(str_filename.Substring(6, 8).Replace("-", "")) <= int.Parse(dt_s.ToString("yyyyMMdd"))))
                    {
                        File.Move(str_folder_begin + @"\" + str_foldername + @"\" + strfilefolder + @"\" + str_filename, str_folder_begin + @"\" + dt_s.ToString("yyyyMMdd") + @"\" + strfilefolder + @"\" + str_filename);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("ex:" + e.ToString() + ", filename=" + str_filename + ", type=" + strtype);
            }
        }
        #endregion

        //#region GoogleAuthenticator
        //private static String GetGACode()
        //{
        //    double totalMilliseconds = DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;

        //    double epoch = totalMilliseconds / 1000.0;
        //    String epochHex = Utilities.Dec2Hex(Math.Floor(epoch / 30), 20).PadLeft(16, '0');

        //    HMACSHA1 hmac = new HMACSHA1();

        //    String hexKey = Utilities.Base32ToDec("AMHHTKAPE7SB5QCMK7XWJKDFRIRFER7P"); //這邊的字串須提供金鑰
        //    hmac.Key = Utilities.HexToByte(hexKey);

        //    byte[] resultArray = hmac.ComputeHash(Utilities.HexToByte(epochHex));
        //    String resultText = Utilities.ByteToString(resultArray);

        //    int offset = Convert.ToInt32(Utilities.Hex2Dec(resultText.Substring(resultText.Length - 1)));

        //    String otp = (Utilities.Hex2Dec(resultText.Substring(offset * 2, 8)) & Utilities.Hex2Dec("7fffffff")).ToString();
        //    otp = (otp).Substring(otp.Length - 6, 6);

        //    return otp;
        //}

        //class Utilities
        //{
        //    public static String Dec2Hex(double value, int maxDecimals)
        //    {
        //        string result = string.Empty;
        //        if (value < 0)
        //        {
        //            result += "-";
        //            value = -value;
        //        }
        //        if (value > ulong.MaxValue)
        //        {
        //            result += double.PositiveInfinity.ToString();
        //            return result;
        //        }
        //        ulong trunc = (ulong)value;
        //        result += trunc.ToString("X");
        //        value -= trunc;
        //        if (value == 0)
        //        {
        //            return result;
        //        }
        //        result += ".";
        //        byte hexdigit;
        //        while ((value != 0) && (maxDecimals != 0))
        //        {
        //            value *= 16;
        //            hexdigit = (byte)value;
        //            result += hexdigit.ToString("X");
        //            value -= hexdigit;
        //            maxDecimals--;
        //        }
        //        return result;
        //    }

        //    public static String Base32ToDec(String base32)
        //    {
        //        String base32chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
        //        String bits = "";
        //        String hex = "";


        //        for (var i = 0; i < base32.Length; i++)
        //        {
        //            int val = base32chars.IndexOf(base32.Substring(i, 1).ToUpper());

        //            bits += Convert.ToString(val, 2).PadLeft(5, '0');
        //        }

        //        for (var i = 0; i + 4 <= bits.Length; i += 4)
        //        {
        //            String chunk = bits.Substring(i, 4);
        //            hex = hex + Convert.ToString(Convert.ToInt32(chunk, 2), 16);
        //        }
        //        return hex;
        //    }

        //    public static byte[] HexToByte(string hexString)
        //    {
        //        byte[] byteOUT = new byte[hexString.Length / 2];
        //        for (int i = 0; i < hexString.Length; i = i + 2)
        //        {
        //            byteOUT[i / 2] = Convert.ToByte(hexString.Substring(i, 2), 16);
        //        }
        //        return byteOUT;
        //    }

        //    public static string ByteToString(byte[] buff)
        //    {
        //        string sbinary = "";

        //        for (int i = 0; i < buff.Length; i++)
        //        {
        //            sbinary += buff[i].ToString("X2"); // hex format
        //        }
        //        return (sbinary);
        //    }

        //    public static int Hex2Dec(String s)
        //    {
        //        return Convert.ToInt32(s, 16);
        //    }

        //}
        //#endregion

        static void Test_command()
        {
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = @"D:\a.bat";
            p.StartInfo.WorkingDirectory = @"D:\";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = false;
            p.Start();

            using (StreamReader myStreamReader = p.StandardOutput)
            {
                string result = myStreamReader.ReadToEnd();
                p.WaitForExit();
                p.Close();
                Console.WriteLine(result);
            }
        }

        static void Testlist2()
        {
            string a = "103,202C";
            string b = "202 ";

            if (a.Split(',').Any(x => x == b))
                Console.WriteLine("true");
        }

        static void Testlist()
        {
            List<GetWCPara> getWC_para = new List<GetWCPara>();

            int int_noofsql = int.Parse(ConfigurationManager.AppSettings["No_Of_SQL"]);


            getWC_para.Clear();

            //20181022 先給10組 不夠用就要再改程式
            for (int i = 0; i < int_noofsql; i++)
            {
                GetWCPara WCparaSet = new GetWCPara();

                WCparaSet.WCSQL = ConfigurationManager.AppSettings["WCSQL" + (i + 1)];
                WCparaSet.WCListtype = ConfigurationManager.AppSettings["WCListtype" + (i + 1)];
                WCparaSet.WCProgram = ConfigurationManager.AppSettings["WCprogram" + (i + 1)];

                getWC_para.Add(WCparaSet);


                Console.WriteLine("WCSQL:" + getWC_para[i].WCSQL);
                Console.WriteLine("WCListtype:" + getWC_para[i].WCListtype);
                Console.WriteLine("WCprogram:" + getWC_para[i].WCProgram);
                Console.WriteLine("WCPara_setCount:" + getWC_para.Count().ToString());
            }

            string query = "";

            for (int i = 0; i < getWC_para.Count(); i++)
            {
                Console.WriteLine("WCpara_count:" + getWC_para.Count().ToString());
                Console.WriteLine("WCSQL:" + getWC_para[i].WCSQL);
                Console.WriteLine("WCListtype:" + getWC_para[i].WCListtype);
                Console.WriteLine("WCProgram:" + getWC_para[i].WCProgram);

                if (getWC_para[i].WCSQL != "")
                {
                    query += getWC_para[i].WCSQL;
                }
            }
        }

        public class GetWCPara
        {
            public string WCSQL { get; set; }
            public string WCListtype { get; set; }
            public string WCProgram { get; set; }
        }

        static string CheckCharacter(string strinput)
        {
            string stroutput = strinput;
            string strtmp = "";
            foreach (char strchar in strinput)
            {
                if (Convert.ToInt32(strchar) < 32 || Convert.ToInt32(strchar) > 126)
                {
                    strtmp += "";
                }
                else
                {
                    strtmp += strchar;
                }
            }

            return stroutput;
        }

        static void CheckMatchText()
        {
            string strMatchtext = "QI YA (CAYMAN) REAL ESTATE";
            string strEngName = "CHINA REAL ESTATE RESEARCH ASSOCIATION";
            string sqlcheck_matchtext = "";
            sqlcheck_matchtext = @"select OFAC.dbo.[OFS_fnGetMatchText](@sSrhName, @sMatchName) ret";
            using (DbCommand dBCommand = database.GetSqlStringCommand(sqlcheck_matchtext))
            {
                database.AddInParameter(dBCommand, "@sSrhName", DbType.String, strMatchtext.Trim());
                database.AddInParameter(dBCommand, "@sMatchName", DbType.String, strEngName.Trim());

                DataSet ds = database.ExecuteDataSet(dBCommand);
                try
                {
                    if (ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
                    {
                        strMatchtext = ds.Tables[0].Rows[0]["ret"].ToString();
                    }
                }
                finally
                {
                    dBCommand.Dispose();
                }
            }
        }
    }
}
